import Link from 'next/link'
import { useRouter } from 'next/router'

export default function HeaderAdmin() {

    const router = useRouter()

    const handleClick = () => {
        localStorage.removeItem('token')
        router.push('/Register/Login/Login')
    }

    return (
        <div className="mx-8 my-4 text-right">
            <button className='px-8 py-2 text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-md hover:bg-teal-500 hover:text-white' onClick={handleClick}>
                Log Out
            </button>
        </div>
    )
}