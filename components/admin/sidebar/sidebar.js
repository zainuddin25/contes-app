import Link from "next/link";
import { useRouter } from 'next/router'

export default function Sidebar (props) {

    const router = useRouter()
    const { id } = router.query

    return(

        <>
        
        <div className="w-96" aria-label="Sidebar">
            <div className="h-full px-3 rounded">
                <div className="px-8 py-4">
                    <Link href="/">
                        <a className="text-2xl font-bold text-black">Just <span className="text-teal-500">Contest</span></a>
                    </Link>
                </div>
                <div className="w-full py-10 text-center">
                    <h1 className="text-lg">{props.title}</h1>
                </div>
                <ul className="px-8">
                    <li>
                        <div className="mb-6">
                            <Link href={`/admin/dashboard/${id}`}>
                                <a className="flex items-center font-normal text-gray-900 rounded-lg text-md hover:text-teal-900">
                                    <i className="w-8 text-l fa-solid fa-users-rectangle"></i>
                                    <span className="ml-3">Dashboard</span>
                                </a>
                            </Link>
                        </div>
                        <div className="mb-6">
                            <Link href={`/admin/contest/${id}`}>
                                <a className="flex items-center font-normal text-gray-900 rounded-lg text-md hover:text-teal-900">
                                    <i className="w-8 text-lg fa-solid fa-trophy"></i>
                                    <span className="ml-3">Contest</span>
                                </a>
                            </Link>
                        </div>
                        <div className="mb-6">
                            <Link href={`/admin/user/${id}`}>
                                <a className="flex items-center font-normal text-gray-900 rounded-lg text-md hover:text-teal-900">
                                    <i className="w-8 text-lg fa-solid fa-circle-user"></i>
                                    <span className="ml-3">Account</span>
                                </a>
                            </Link>
                        </div>
                    </li>
                    <hr />
                    <li>
                        <div className="mt-4 mb-6">
                            <Link href={`/admin/payment/contestor/${id}`}>
                                <a className="flex items-center font-normal text-gray-900 rounded-lg text-md hover:text-teal-900">
                                    <i className="w-8 text-lg fa-solid fa-money-bill-1-wave"></i>
                                    <span className="ml-3">Contestor Payment</span>
                                </a>
                            </Link>
                        </div>
                        <div className="mt-4 mb-6">
                            <Link href={`/admin/payment/contestan/${id}`}>
                                <a className="flex items-center font-normal text-gray-900 rounded-lg text-md hover:text-teal-900">
                                    <i className="w-8 text-lg fa-solid fa-money-bill-1-wave"></i>
                                    <span className="ml-3">Contestan Payment</span>
                                </a>
                            </Link>
                        </div>
                    </li>
                    <hr />
                    <li>
                        <div className="mt-4 mb-6">
                            <Link href={`/admin/report/${id}`}>
                                <a className="flex items-center font-normal text-gray-900 rounded-lg text-md hover:text-teal-900">
                                    <i className="w-8 text-lg fa-solid fa-file-arrow-down"></i>
                                    <span className="ml-3">Report</span>
                                </a>
                            </Link>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        </>

    )
}