import Link from "next/link"

export default function Footer() {
    return(
        <>
            <div className="w-full py-16 bg-slate-900">
                <div className="flex flex-col w-3/4 mx-auto mb-10 lg:flex-row">
                    <div className="mb-2 lg:w-1/4 lg:pl-2">
                        <Link href="/">
                            <a className="text-xl font-bold text-white">Just<span className="text-teal-500">Contest</span></a>
                        </Link>
                        <p className="pt-2 text-sm text-white">Just Contest is a contest website that has many contests that have high nominal prizes</p>
                    </div>
                    <div className="mb-2 lg:w-1/4 lg:pl-5">
                        <h1 className="text-lg font-semibold text-white">Explore</h1>
                        <div className="flex flex-col">
                            <span><Link href="#home"><a className="text-white font-base hover:text-teal-500">Home</a></Link></span>
                            <span><Link href="#about"><a className="text-white font-base hover:text-teal-500">About US</a></Link></span>
                            <span><Link href="#tutorial"><a className="text-white font-base hover:text-teal-500">How To Join</a></Link></span>
                            <span><Link href="#contact"><a className="text-white font-base hover:text-teal-500">Contact US</a></Link></span>
                            <span><Link href="./Register/Login/Login"><a className="text-white font-base hover:text-teal-500">Log In</a></Link></span>
                        </div>
                    </div>
                    <div className="mb-2 lg:w-1/4 lg:pl-2">
                        <h1 className="text-lg font-semibold text-white">Comunity</h1>
                        <div className="flex flex-col text-white">
                            <span className="cursor-pointer hover:text-teal-500"><i className="fa-brands fa-whatsapp pr-2 w-[24px] mx-auto"></i> Whatsapp Grub</span>
                            <span className="cursor-pointer hover:text-teal-500"><i className="fa-brands fa-discord pr-2 w-[24px] mx-auto"></i> Discord Channel</span>
                            <span className="cursor-pointer hover:text-teal-500"><i className="fa-brands fa-youtube pr-2 w-[24px] mx-auto"></i> Youtube</span>
                            <span className="cursor-pointer hover:text-teal-500"><i className="fa-brands fa-instagram pr-2 w-[24px] mx-auto"></i> Instagram Tag</span>
                            <span className="cursor-pointer hover:text-teal-500"><i className="fa-brands fa-facebook-f pr-2 w-[24px] mx-auto"></i> Facebook Grub</span>
                        </div>
                    </div>
                    <div className="mb-2 lg:w-1/4 lg:pl-2">
                        <h1 className="text-lg font-semibold text-white">Service</h1>
                        <div className="flex flex-col">
                            <span className="text-sm font-semibold text-white cursor-pointer hover:text-teal-500"><i className="fa-solid fa-phone w-[24px]"></i> +62 8515 6970 093</span>
                            <span className="text-sm font-semibold text-white cursor-pointer hover:text-teal-500"><i className="fa-solid fa-envelope w-[24px]"></i> justcontest@gmail.com</span>
                            <span className="text-sm font-semibold text-white cursor-pointer hover:text-teal-500"><i className="fa-solid fa-location-dot w-[24px]"></i> Indonesia, Central Java</span>
                        </div>
                    </div>
                </div>
                <hr className="w-3/4 mx-auto" />
                <div className="flex w-3/4 mx-auto">
                    <div className="flex items-center justify-center w-full pt-10 mx-auto">
                        <span className="border cursor-pointer rounded-full mx-1 text-white py-3 px-[18px]  hover:bg-teal-500 hover:border-teal-500"><i className="fa-brands fa-facebook-f"></i></span>
                        <span className="border cursor-pointer rounded-full mx-1 text-white py-3 px-[17px]  hover:bg-teal-500 hover:border-teal-500"><i className="fa-brands fa-instagram"></i></span>
                        <span className="px-4 py-3 mx-1 text-white border rounded-full cursor-pointer hover:bg-teal-500 hover:border-teal-500"><i className="fa-brands fa-twitter"></i></span>
                        <span className="px-4 py-3 mx-1 text-white border rounded-full cursor-pointer hover:bg-teal-500 hover:border-teal-500"><i className="fa-brands fa-github"></i></span>
                        <span className="px-4 py-3 mx-1 text-white border rounded-full cursor-pointer hover:bg-teal-500 hover:border-teal-500"><i className="fa-brands fa-gitlab"></i></span>
                        <span className="px-4 py-3 mx-1 text-white border rounded-full cursor-pointer hover:bg-teal-500 hover:border-teal-500"><i className="fa-brands fa-youtube"></i></span>
                    </div>
                </div>
            </div>
        </>
    )
}