import Head from "next/head"

export default function Layout(props) {
    return(
        <Head>
            <title>JustContest - { props.pageTitle }</title>
        </Head>
    )
}