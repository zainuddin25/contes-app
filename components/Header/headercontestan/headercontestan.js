import { useEffect, useState } from "react";
import Link from "next/link"
import Image from "next/image"
import { Dropdown, Menu, Space, message } from 'antd';
import { useRouter } from "next/router";
import jwtDecode from "jwt-decode";
import axios from "axios";


export default function HeaderContestan() {
    const [data, setData] = useState()
    const [idUser, setIdUser] = useState([])

    const router = useRouter()

    const handleClick = () => {
        try{
            localStorage.removeItem('token')
            router.push('/Register/Login/Login')
            message.success("Sign Out Success")
        }catch (err) {
            message.error('Ups, something wrong')
        }
    }

    const apiGetProfile = async () => {
        try{
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            setIdUser(decode.sub)
            axios.get(`http://localhost:3222/users/${decode.sub}`)
            .then((response) => {
                setData(response.data.data)
            })
        } catch (err) {
            console.log("Error :",err)
        }
    }

    const loaderProp =({ src }) => {
        return src;
    }

    useEffect(() => {
        apiGetProfile()
    }, [])

    const menu = (
        <Menu
            items={[
                {
                    label: 
                    <>
                        <Link href={`/contestanfitur/contestancontest/${idUser}}`}>
                            <a>List Contest</a>
                        </Link>
                    </>,
                    key: 0
                },
                {
                    label: 
                    <>
                        <Link href={`/contestanfitur/mycontest/${idUser}`}>
                            <a>Curent Contest</a>
                        </Link>
                    </>,
                    key: 1,
                },
            ]}
        />
    )

    const menuProfile = (
        <Menu
          items={[
            {
              key: '1',
              label: (
                <>
                    <Link href={`/contestan/profile/${idUser}`}>
                        <a>Profile</a>
                    </Link>
                </>
              ),
            },
            {
              key: '2',
              label: (
                <>
                    <Link href={`/contestan/update/${idUser}`}>
                        <a>Update to contestor</a>
                    </Link>
                </>
              ),
            },
            {
                type: 'divider'
            },
            {
              key: '3',
              label: (
                <>
                    <h1 className="cursor-pointer" onClick={handleClick}>
                        Sign Out
                    </h1>
                </>
              ),
            },
          ]}
        />
    );

    return(
        <>
            <div className="fixed z-50 w-screen bg-white background-blur-sm border-gray">
                <div className="container flex justify-between mx-auto my-2">
                    <div className="w-1/4 pl-5">
                        <Link href="/">
                            <a className="flex items-center pt-1 text-2xl font-bold text-black">
                                Just
                                <span className="pl-1 text-teal-500">
                                    Contest
                                </span>
                            </a>
                        </Link>
                    </div>
                    <div className="hidden w-full lg:w-2/4 lg:block top-14 lg:bg-white bg-slate-200">
                        <ul className="flex flex-col justify-center pt-2 lg:flex-row">
                            <li className="px-4 pt-1 lg:pt-0">
                                <Link href={`/contestanfitur/contestancontest/${idUser}`}>
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500 active:text-teal-500">List Contest</a>
                                </Link>
                            </li>
                            <li className="px-4 py-1 lg:py-0">
                                <Link href={`/contestanfitur/mycontest/${idUser}`}>
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500 active:text-teal-500">Curent Contest</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="flex justify-end w-2/4 pr-5 lg:w-1/4">
                        <Dropdown overlay={menu} trigger={['click']}>
                            <a onClick={(e) => e.preventDefault()}>
                            <Space>
                                <button className="p-2 mt-1 ml-3 mr-3 border border-teal-500 rounded-lg lg:hidden">
                                    <svg className="w-5 h-5" aria-hidden="true" fill="black" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd"></path>
                                    </svg>
                                </button>
                            </Space>
                            </a>
                        </Dropdown>
                        <Dropdown overlay={menuProfile} placement="bottomRight">
                            {/* <Image src={profile} className="rounded-full cursor-pointer" width={50} height={50} /> */}
                            <Image src={`http://localhost:3222/files/${data?.photoProfile}`} alt="Photo Profile" className="rounded-full" width={50} height={50} />
                        </Dropdown>
                    </div>
                </div>
            </div>
        </>
    )
}