import Link from "next/link"
import { useState } from "react"

export default function Header() {

    const [statusNav, setStatusNav] = useState('hidden')

    const navbarStatus = () => {
        if(statusNav === 'hidden') {
            setStatusNav('absolute');
        }else{
            setStatusNav('hidden');
        }
    }

    return (
        <>
            <div className="fixed z-50 w-screen bg-white opacity-90 background-blur-sm border-gray">
                <div className="container flex justify-between mx-auto my-2">
                    <div className="w-1/4 pl-5">
                        <Link href="/">
                            <a className="flex items-center pt-1 text-2xl font-bold text-black">
                                Just
                                <span className="pl-1 text-teal-500">
                                    Contest
                                </span>
                            </a>
                        </Link>
                    </div>
                    <div className={`${statusNav} w-full lg:w-2/4 lg:block top-14 lg:bg-white bg-slate-200`}>
                        <ul className="flex flex-col justify-center pt-2 lg:flex-row">
                            <li className="px-4 pt-1 lg:pt-0">
                                <Link href="#home">
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500">Home</a>
                                </Link>
                            </li>
                            <li className="px-4 py-1 lg:py-0">
                                <Link href="#about">
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500">About US</a>
                                </Link>
                            </li>
                            <li className="px-4 py-1 lg:py-0">
                                <Link href="#tutorial">
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500">How To Join</a>
                                </Link>
                            </li>
                            <li className="px-4 pt-1 pb-4 lg:pt-0 lg:pb-0">
                                <Link href="#contact">
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500">Contact US</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="flex justify-end w-2/4 pr-5 lg:w-1/4">
                        <Link href="./Register/Login/Login">
                            <button className="flex float-right px-8 py-2 mt-1 text-sm text-teal-500 duration-100 ease-in border border-teal-500 rounded-lg hover:bg-teal-500 hover:text-white hover:border-white lg:bg-white lg:opacity-100">
                                    Login
                            </button>
                        </Link>
                        <button onClick={navbarStatus} className="p-2 mt-1 ml-3 border border-teal-500 rounded-lg lg:hidden">
                            <svg className="w-5 h-5" aria-hidden="true" fill="black" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd"></path>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}