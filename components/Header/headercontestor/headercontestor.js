import { useEffect, useState } from "react";
import Link from "next/link"
import { Dropdown, Menu, Space } from 'antd';
import { useRouter } from "next/router";
import jwtDecode from "jwt-decode";
import axios from 'axios'


export default function HeaderContestor() {

    const [photoProfile, setPhotoProfile] = useState()
    const [idUser, setIdUser] = useState([])

    const router = useRouter()
    const { id } = router.query

    const handleClick = () => {
        localStorage.removeItem('token')
        router.push('/Register/Login/Login')
    }

    const apiGetProfile = async () => {
        try{
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            setIdUser(decode.sub)
            axios.get(`http://localhost:3222/users/${decode.sub}`)
            .then((response) => {
                setPhotoProfile(response.data.data.photoProfile)
            })
        } catch (err) {
            console.log("Error :",err)
        }
    }

    useEffect(() => {
        apiGetProfile()
    }, [])

    const menu = (
        <Menu
            items={[
                {
                    label: 
                    <>
                        <Link href={`/contestorfitur/contestorcontest/${idUser}}`}>
                            <a>List Contest</a>
                        </Link>
                    </>,
                    key: 0
                },
                {
                    label: 
                    <>
                        <Link href={`/contestorfitur/mycontest/${idUser}`}>
                            <a>Curent Contest</a>
                        </Link>
                    </>,
                    key: 1,
                },
                {
                    label: 
                    <>
                        <Link href={`/contestorfitur/global/${idUser}`}>
                            <a>Global</a>
                        </Link>
                    </>,
                    key: 2,
                },
                {
                    label: 
                    <>
                        <Link href={`/contestorfitur/contestcreated/${idUser}`}>
                            <a>Created Contest</a>
                        </Link>
                    </>,
                    key: 3,
                },
                {
                    label: 
                    <>
                        <Link href={`/contestorfitur/contestcreated/payment/${idUser}`}>
                            <a>Event Payment</a>
                        </Link>
                    </>,
                    key: 4,
                },
            ]}
        />
    )

    const menuProfile = (
        <Menu
          items={[
            {
              key: '1',
              label: (
                <>
                    <Link href={`/Contestor/profile/${idUser}`}>
                        <a>Profile</a>
                    </Link>
                </>
              ),
            },
            {
                type: 'divider'
            },
            {
              key: '2',
              label: (
                <>
                    <h1 className="cursor-pointer" onClick={handleClick}>
                        Sign Out
                    </h1>
                </>
              ),
            },
          ]}
        />
    );

    return(
        <>
            <div className="fixed z-50 w-screen bg-white background-blur-sm border-gray">
                <div className="container flex justify-between mx-auto my-2">
                    <div className="w-1/4 pl-5">
                        <Link href="/">
                            <a className="flex items-center pt-1 text-2xl font-bold text-black">
                                Just
                                <span className="pl-1 text-teal-500">
                                    Contest
                                </span>
                            </a>
                        </Link>
                    </div>
                    <div className="hidden w-full lg:w-2/4 lg:block top-14 lg:bg-white bg-slate-200">
                        <ul className="flex flex-col justify-center pt-2 lg:flex-row">
                            <li className="px-4 pt-1 lg:pt-0">
                                <Link href={`/contestorfitur/contestorcontest/${id}`}>
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500 active:text-teal-500">List Contest</a>
                                </Link>
                            </li>
                            <li className="px-4 py-1 lg:py-0">
                                <Link href={`/contestorfitur/mycontest/${idUser}`}>
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500 active:text-teal-500">Curent Contest</a>
                                </Link>
                            </li>
                            <li className="px-4 py-1 lg:py-0">
                                <Link href={`/contestorfitur/global/${idUser}`}>
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500 active:text-teal-500">Global</a>
                                </Link>
                            </li>
                            <li className="px-4 py-1 lg:py-0">
                                <Link href={`/contestorfitur/contestcreated/${idUser}`}>
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500 active:text-teal-500">Created Contest</a>
                                </Link>
                            </li>
                            <li className="px-4 py-1 lg:py-0">
                                <Link href={`/contestorfitur/contestcreated/payment/${idUser}`}>
                                    <a className="font-normal lg:font-medium text-slate-700 hover:text-teal-500 active:text-teal-500">Events Payment</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="flex justify-end w-2/4 pr-5 lg:w-1/4">
                        <Dropdown overlay={menu} trigger={['click']}>
                            <a onClick={(e) => e.preventDefault()}>
                            <Space>
                                <button className="p-2 mt-1 ml-3 mr-3 border border-teal-500 rounded-lg lg:hidden">
                                    <svg className="w-5 h-5" aria-hidden="true" fill="black" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd"></path>
                                    </svg>
                                </button>
                            </Space>
                            </a>
                        </Dropdown>
                        <Dropdown overlay={menuProfile} placement="bottomRight">
                            <img src={`http://localhost:3222/files/${photoProfile}`} className="rounded-full" width={50} height={50} />
                        </Dropdown>
                    </div>
                </div>
            </div>
        </>
    )
}