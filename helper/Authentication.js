import { useRouter } from "next/router";

const useAuthenicatedPage = () => {
    const router = useRouter()
    const ISSERVER = typeof window === 'undefined'
    if(!ISSERVER) {
        if(!localStorage.getItem('token')) {
            window.location.href = '/Register/Login/Login'
        }
    }
}

export default useAuthenicatedPage