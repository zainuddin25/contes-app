import { Button, Card, Modal } from 'antd';
import React, { useEffect, useState } from 'react';
import Header from "../../../components/Header/headercontestan/headercontestan"
import Layout from "../../../components/Layout/Layout"
import Footer from "../../../components/Footer/Footer"
import Image from 'next/image';
import Link from "next/link"
import axios from 'axios'
import { useRouter } from 'next/router';
import useAuthenicatedPage from '../../../helper/Authentication';
import jwtDecode from 'jwt-decode'

export default function Detail() {

    const [events, setEvents] = useState([])
    const [contestor, setContestor] = useState([])
    const [winner, setWinner] = useState([])
    const [price, setPrice] = useState([])
    const [modal1Visible, setModal1Visible] = useState(false);
    const [modal2Visible, setModal2Visible] = useState(false);
    const [dataSubmit, setDataSubmit] = useState([{
        linkPath: '',
    }])
    
    const router = useRouter()

    useAuthenicatedPage()

    const getDetailContest = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/events/find/${id}`)
            .then((response) => {
                const detail = response.data.data
                const result = detail.user
                setEvents(detail)
                const tempPrice = detail.competitionPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                setPrice(tempPrice)
                setContestor(result)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    const getWinner = async () => {
        try{
            const { id } = router.query
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            await axios.get(`http://localhost:3222/contestans/winner/${id}`)
            .then((response) => {
                setWinner(response.data.data)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    const getSubmitData = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/contestans/event/submit?id=${id}`)
            .then((response) => {
                setDataSubmit(response.data.data)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    const handelDownload = () => {
        try{
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            const idUser = winner?.user?.id
            if(decode.sub === idUser) {
                setModal1Visible(true)
            } else {
                setModal2Visible(true)
            }
        }catch(err){
            console.log('Error : ', err)
        }
    }

    const info = () => {
        Modal.info({
          title: "Oops, the contest looks like it's over",
          content: (
            <div>
              <p>Please look for other ongoing contests</p>
            </div>
          ),
      
          onOk() {},
        });
    };

    const statusContest = () => {
        if(events.isActive === true) {
            router.push(`/contestanfitur/joincontest/${events.id}`)
        } else {
            info()
        }
    }

    const winnerS = () => {
        if(winner === null) {
            return (
                <div className='w-full text-center'>
                    <h1>The contest is running, there is still a chance for you to win this contest</h1>
                </div>
            )
        } else {
            return (
                <>
                    <div className="w-4/12 mx-auto">
                        <div className="w-full my-2">
                            <div className="flex flex-col justify-center mt-8 lg:mt-0">
                                <div className="w-full rounded-lg shadow-lg">
                                    <div className="flex justify-center w-full rounded-lg">
                                        <Image src={`http://localhost:3222/files/${winner?.linkPath}`} className='relative flex items-center justify-center' style={{borderTopRightRadius: "5px", borderTopLeftRadius: "5px"}} width={400} height={400}/>
                                    </div>
                                    <div className='flex justify-between'>
                                        <span className='px-4 py-2 text-base'><i className="mr-2 fa-solid fa-circle-user"></i> {winner?.user?.username}</span>
                                        <span className='px-4 py-2'>{winner?.status}</span>
                                    </div>
                                </div>
                                <div style={{
                                    textAlign: 'center',
                                    marginTop: "50px"
                                }}>
                                    <Link href={`/certificate/${winner?.id}`}>
                                        <p onClick={handelDownload} className='py-2 text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-md cursor-pointer hover:bg-teal-500 hover:text-white'>Downlad Certificate</p>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            )
        }
    }
    
    useEffect(() => {
        getDetailContest()
        getSubmitData()
        getWinner()
    }, [])

    const tabListNoTitle = [
        {
            key: 'brief',
            tab: 'Brief',
        },
        {
            key: 'submits',
            tab: 'Submits',
        },
        {
            key: 'winner',
            tab: 'Winner',
        },
    ];

    const contentListNoTitle = {
        brief: 
        <>
            <div>
                <h1 className="text-2xl font-bold">Descroption Contest</h1>
                <p>{events.description}</p>
            </div>
        </>,
        submits: 
        <>
            <div className="flex flex-col px-8 py-10 mx-auto rounded-sm lg:grid lg:grid-cols-4 lg:gap-4" style={{
                boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.1)'
            }}>
                {dataSubmit?.map((values) => {
                    return (
                        <>
                        <div className="w-full my-2" key={values?.id}>
                            <div className="flex justify-center mt-8 lg:mt-0">
                                <div className="w-full rounded-lg shadow-lg">
                                    <div className="flex justify-center w-full rounded-lg">
                                        <Image className='relative flex items-center justify-center' style={{borderTopRightRadius: "5px", borderTopLeftRadius: "5px"}} src={`http://localhost:3222/files/${values?.linkPath}`} width={300} height={300}/>
                                    </div>
                                    <div className='flex justify-between'>
                                        <span className='px-4 py-2 text-base'><i className="mr-2 fa-solid fa-circle-user"></i> {values?.user?.username}</span>
                                        <span className='px-4 py-2'>{values?.status}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </>
                    )
                })}
            </div>
        </>,
        winner: 
            winnerS()
    };

    const [activeTabKey2, setActiveTabKey2] = useState('submits');

    const onTab2Change = (key) => {
        setActiveTabKey2(key);
    };

    return(
        <>
            <Layout pageTitle={"Contest Detail"} />
            <Header />
            <div className="pt-20 pb-10">
                <div className="flex justify-between w-full mx-auto lg:px-20">
                    <div className="w-3/4 px-4">
                        <h1 className="mb-3 text-2xl font-bold">{events.title}</h1>
                        <p className="font-semibold"><span className="pr-2 font-normal"><i>From</i></span>{contestor.username}</p>
                    </div>
                    <div className="w-1/4 mr-2">
                        <p className="font-bold text-right">Rp{price},00</p>
                        <button onClick={statusContest} className="w-full py-2 text-xs text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-md lg:w-2/4 lg:float-right hover:bg-teal-500 hover:border-teal-500 hover:text-white">
                            Upload
                        </button>
                    </div>
                </div>
                <div className="container mx-auto">
                    <div className="mx-4">
                        <Card bordered={false} style={{ width: '100%' }} tabList={tabListNoTitle} activeTabKey={activeTabKey2} onTabChange={(key) => { onTab2Change(key) }}>{contentListNoTitle[activeTabKey2]}</Card>
                    </div>
                </div>
                <Modal title="Confirmasi" style={{ top: 20, }} visible={modal1Visible} 
                    footer={[
                        <Button onClick={() => setModal1Visible(false)}>Cancel</Button>,
                        <Button onClick={() => setModal1Visible(false)} type="primary">
                            <a onClick={handelDownload}>
                                Download Certificate
                            </a>
                        </Button>
                    ]}
                >
                    <p>
                        Make sure you have included your email and account number in your profile. Because the admin will send the prize 
                        money via the account number listed in the profile and proof of payment will be sent to the email listed on the profile. 
                        Admin will wait for 1x24 hours if the winner's account number and email are wrong, it's not admin's responsibility
                    </p>
                </Modal>
                <Modal title="Confirmasi" style={{ top: 20, }} visible={modal2Visible} 
                    footer={[
                        <Button onClick={() => setModal2Visible(false)}>Close</Button>,
                    ]}
                >
                    <p>Sorry you are not winner</p>
                </Modal>
            </div>
            <Footer />
        </>
    )
}