import { useRouter } from "next/router"
import Layout from "../../../components/Layout/Layout"
import { UploadOutlined } from '@ant-design/icons';
import { Button, Form, Input, Upload, message } from 'antd';
import React, { useEffect, useState } from 'react';
import axios from 'axios'
import Image from 'next/image'
import useAuthenicatedPage from "../../../helper/Authentication";
const { TextArea } = Input;

export default function JoinContest () {

    const [fileList, setFileList] = useState([])
    const [path, setPath] = useState([])
    const [idContest, setIdContest] = useState([])
    const [event, setEvent] = useState([])

    useAuthenicatedPage()

    const router = useRouter()

    useEffect(() => {
        getDataContest()
    }, [])

    const handlerImageUpload = async (args) => {
        try{
            const token = localStorage.getItem('token')
            const formData = new FormData()
            formData.append("file", args.file)
            await axios.post('http://localhost:3222/files/contestan', formData, {
                headers: {
                    'Authorization' : `Bearer ${token}`,
                }
            })
            .then((response) => {
                setFileList([
                    {
                        path: response.data.data,
                        status: 'done',
                        name: 'Upload Success'
                    }
                ])
                setPath(response.data)
                if(response.status === 201) {
                    message.success('Image Uploaded')
                }
            })
        } catch (err) {
            console.log("Wop, Error : ", err)
        }
    }

    const submitContest = async (values) => {
        const token = localStorage.getItem('token')
        const { id } = router.query
        try{
            await axios.post(`http://localhost:3222/contestans?event=${id}`, {
                title: values.title,
                description: values.description,
                linkPath: path.data
            }, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                if(response.status === 201) {
                    message.success('Submit Success')
                    router.push(`/contestanfitur/contestandetail/${idContest}`)
                }
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    const getDataContest = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/events/find/${id}`)
            .then((response) => {
                setIdContest(response.data.data.id)
                setEvent(response.data.data)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

 
    const handleCancel = () => {
        message.destroy('Sumbit Cancel')
        router.push(`/contestanfitur/contestandetail/${idContest}`)
    }
    
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return(
        <>
            <Layout pageTitle="Join Contest" />
            <div className="flex w-screen h-screen">
                <div className="w-1/4 bg-slate-800">
                    <div className="mx-6 mt-8">
                        <h1 className="text-lg text-white">Title Contest</h1>
                        <p className="text-white text-md">- {event.title}</p>
                        <h1 className="text-lg text-white">Description Contest</h1>
                        <p className="w-2/3 text-white text-md">{event.description}</p>
                    </div>
                </div>
                <div className="flex flex-col items-center justify-center w-2/4 h-full rounded-sm">
                    <div className='mb-8 shadow-md'>
                        <Image src={`http://localhost:3222/files/${path?.data}`} width={300} height={300} />
                    </div>
                    <Upload customRequest={handlerImageUpload} fileList={fileList}>
                        <Button style={{background: "transparent"}} icon={<UploadOutlined />}>Click to Upload Your Image</Button>
                    </Upload>
                </div>
                <div className="w-1/4 bg-slate-800">
                    <div className="mx-6 mt-8">
                        <p className="text-white">Upload Image</p>
                        <div className="pt-10">
                            <Form name="basic" onFinish={submitContest} onFinishFailed={onFinishFailed} autoComplete="off">
                                <h1 className="text-white">Title</h1>
                                <Form.Item name="title" rules={[{ required: true, message: 'Please input your title!' }]}>
                                    <Input style={{background: "transparent", color: "white"}} />
                                </Form.Item>

                                <h1 className="text-white">Description</h1>
                                <Form.Item name="description">
                                    <TextArea rows={10} style={{background: "transparent", color: "white"}} />
                                </Form.Item>

                                <div className="flex">
                                    <Form.Item>
                                        <Button type="primary" htmlType="submit">
                                            Submit
                                        </Button>
                                    </Form.Item>
                                    <Button onClick={handleCancel} style={{marginLeft: "10px", background: "transparent", color: "white"}}>Cancel</Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}