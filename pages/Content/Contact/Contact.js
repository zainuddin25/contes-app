import { Button, Form, Input, Select } from 'antd';
import React, { useEffect, useState } from 'react';


export default function Content() {
    const [form] = Form.useForm();
    const [, forceUpdate] = useState({}); // To disable submit button at the beginning.

    useEffect(() => {
        forceUpdate({});
    }, []);

    const onFinish = (values) => {
        console.log('Finish:', values);
    };


    return(
        <>
            <div className="w-full h-screen py-6 mx-auto lg:pt-12 lg:px-56 bg-slate-100" id="contact">
                <div className="w-3/4 pt-10 mx-auto text-center lg:w-full lg:mb-12">
                    <h1 className="text-sm font-semibold tracking-wide text-center text-teal-500 uppercase">- Contact US -</h1>
                </div>
                <div className="flex flex-col lg:flex-row">
                    <div className="w-3/4 mx-auto lg:w-2/4">
                        <p className="w-3/4 pt-3 pb-2 mx-auto text-xs text-center lg:text-base lg:text-left lg:mx-0">
                            Contact us when you have trouble or a comment, your suggestions and comments mean a lot to us
                        </p>
                        <h1 className="text-lg font-bold">CS Admin</h1>
                        <p className="text-sm font-semibold"><i className="pr-1 fa-solid fa-phone"></i> +62 8515 6970 093</p>
                        <p className="text-sm font-semibold"><i className="pr-2 fa-solid fa-envelope"></i> justcontest@gmail.com</p>
                        <p className="text-sm font-semibold"><i className="pr-2 fa-solid fa-location-dot"></i> Indonesia, Central Java</p>
                    </div>
                    <hr className="w-3/4 mx-auto my-3 lg:w-2/4 lg:hidden" />
                    <div className="pt-4 mx-auto full lg:w-2/4">
                        <Form form={form} name="horizontal_login" onFinish={onFinish}>
                            <div className="flex">
                                <Form.Item name="username" rules={[{ required: true, message: 'Please input your username!' }]} style={{paddingRight: 2, width: "50%"}}>
                                    <Input placeholder="Username" type= 'text' />
                                </Form.Item>
                                <Form.Item name="email" rules={[{ required: true, message: 'Please input your email!' }]} style={{paddingLeft: 2, width: "50%"}}>
                                    <Input placeholder="Email" type= 'email' />
                                </Form.Item>
                            </div>
                            <div className="w-full">
                                <Form.Item name="comment" rules={[{ required: true, message: 'Please input comment!' }]}>
                                    <Input.TextArea placeholder='Your Message' showCount maxLength={300} style={{height: "200px"}}/>
                                </Form.Item>
                            </div>
                            <div className="flex justify-center mx-auto">
                                <Form.Item shouldUpdate>
                                    {() => (
                                    <Button type="primary" htmlType="submit" disabled={ !form.isFieldsTouched(true) || !!form.getFieldsError().filter(({ errors }) => errors.length).length } style={{width: "200px"}}>
                                        Submit
                                    </Button>
                                    )}
                                </Form.Item>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        </>
    )
}