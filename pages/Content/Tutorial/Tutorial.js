import Image from 'next/image'
import { Card } from 'antd';
import tutorial from "./../../../public/img/tutorial.png"
import React, { useState } from 'react';


export default function Tutorial() {

    const [activeTabKey2, setActiveTabKey2] = useState('tutor1');

    const onTab2Change = (key) => {
        setActiveTabKey2(key);
    };

    const tabListNoTitle = [
      {
        key: 'tutor1',
        tab: 'How To Registration',
      },
    ];

    const contentListNoTitle = {
      tutor1: 
      <>
        <div className="flex flex-col lg:flex-row">
            <div className="lg:w-2/4 sm:w-full">
                <Image src={tutorial}/>
            </div>
            <div className="lg:w-2/4 lg:px-4 lg:pt-2">
              <h1 className="text-xl text-center">How To Register</h1>
              <p className="lg:text-base">
                  1. Click the login button <br />
                  2. Enter the requested data <br />
                  3. If you don't have an account. Please press register now. <br />
                  4. Enter your data and don't forget!. <br />
                  5. When you forget your password, please enter the forgot password page by pressing forgot password. <br />
                  6. Enter the recovery email, you will be sent a link and press the link to continue registration. <br /> 
              </p>
            </div>
        </div>
      </>,
    };


    return(
        <>
            <div className='w-full h-full pt-10 mx-auto lg:w-3/4 lg:h-screen lg:pt-20' id="tutorial">
                <h1 className="py-2 mb-3 text-sm font-bold text-center text-teal-500 uppercase">- How To Join -</h1>
                <Card style={{ width: '100%' }} tabList={tabListNoTitle} activeTabKey={activeTabKey2} onTabChange={(key) => { onTab2Change(key) }}>
                    {contentListNoTitle[activeTabKey2]}
                </Card>
            </div>
        </>
    )
}