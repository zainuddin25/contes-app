export default function About() {
    return(
        <>
            <div className="w-full h-auto py-10 bg-slate-100 lg:h-screen lg:py-20" id="about">
                <h1 className="text-sm font-semibold tracking-wide text-center text-teal-500 uppercase">- About -</h1>
                <p className="max-w-[75%] py-10 text-xs mx-auto text-center lg:text-base lg:w-2/4">
                    Developed in 2022. To date we have 100 users, and 100 total contests. We also provide many 
                    facilities, including being able to multi-user or being able to create multiple accounts with
                    one device. Multi content, each contestant can submit and participate in more than one contest 
                    thereby increasing your chances of getting prizes from our contests. Portfolio, if you win one 
                    of the available contests, it will be entered into your profile and can be used for your portfolio
                </p>
                <div className="flex flex-col w-full mx-auto lg:w-3/4 lg:flex-row">
                    <div className="w-[60%] lg:w-1/3 lg:mr-2 lg:mb-0 mb-2 mx-auto border rounded-md border-teal-500 text-center">
                        <p className="py-3 text-4xl">
                            <i className="p-5 text-white duration-200 ease-in-out bg-teal-500 border border-teal-500 rounded-full fa-solid fa-circle-user hover:bg-slate-100 hover:text-teal-500 hover:border-slate-100"></i>
                        </p>
                        <h1 className="py-2 text-xl font-semibold uppercase">Multiple User</h1>
                        <p className="px-5 pb-5 text-sm">One user can create more than one account</p>
                    </div>

                    <div className="w-[60%] lg:w-1/3 lg:mx-2 lg:my-0 my-2 mx-auto border rounded-md border-teal-500 text-center">
                        <p className="py-3 text-4xl">
                            <i className="p-5 text-white duration-200 ease-in-out bg-teal-500 border border-teal-500 rounded-full fa-solid fa-medal hover:bg-slate-100 hover:text-teal-500 hover:border-slate-100"></i>
                        </p>
                        <h1 className="py-2 text-xl font-semibold uppercase">Multiple Contest</h1>
                        <p className="px-5 pb-5 text-sm">One user can participate in many contests</p>
                    </div>

                    <div className="w-[60%] lg:w-1/3 lg:ml-2 lg:mt-0 mt-2 mx-auto border rounded-md border-teal-500 text-center">
                        <p className="py-3 text-4xl">
                            <i className="fa-solid fa-address-card border px-5 py-[22px] rounded-full border-teal-500 text-white bg-teal-500 hover:bg-slate-100 hover:text-teal-500 hover:border-slate-100 ease-in-out duration-200"></i>
                        </p>
                        <h1 className="py-2 text-xl font-semibold uppercase">Portfolio</h1>
                        <p className="px-5 pb-5 text-sm">User can create a portfolio on this website</p>
                    </div>
                </div>
            </div>
        </>
    )
}