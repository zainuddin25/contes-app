import Image from 'next/image'
import Link from 'next/link'
import logo from './../../../public/logo.png'

export default function Hero() {

    return(
        <>
            <div className="flex flex-col items-center justify-center w-full h-screen" id="home">
                <Image src={logo} width={100} height={100} />
                <h1 className="py-4 text-4xl font-bold tracking-wider uppercase">Welcome</h1>
                <p className="max-w-[75%] lg:w-2/4 lg:text-base mx-auto text-center text-sm">
                    <span className="pr-1 font-bold">JustContest</span>
                    is a contest website that has many contests that have high nominal prizes. followed by 
                    many users and the number of contests in one day is the advantage of our website. the ease
                    of participating in a contest makes the contestants or users on our website happy. 
                    Start freelance by taking part in the contests that we provide on our website. only 
                    with a signal and your creativity can make money
                </p>
                <Link href="./Register/Login/Login">
                    <button className="px-8 py-3 mt-4 text-teal-500 duration-200 ease-in-out border border-teal-500 hover:text-white hover:border-white hover:bg-teal-500 rounded-xl">
                        Search Contest
                    </button>
                </Link>
            </div>
        </>
    )
}