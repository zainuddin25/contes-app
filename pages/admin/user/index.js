import Sidebar from '../../../components/admin/sidebar/sidebar' 
import Layout from '../../../components/Layout/Layout'
import { Space, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import axios from 'axios'
import { useRouter } from 'next/router';
import useAuthenicatedPage from '../../../helper/Authentication';

export default function User() {

  const router = useRouter()
  const { id } = router.query

  useAuthenicatedPage()

  const [datas, setDatas] = useState([])

  const getDataAccount = async() => {
    try{
      await axios.get('http://localhost:3222/users')
      .then((response) => {
        setDatas(response.data.data)
      })
    }catch(err){
      console.log("Error : ", err)
    }
  }

  useEffect(() => {
      getDataAccount()
  },[])

  const columns = [
    {
      title: 'Username',
      dataIndex: 'username',
      key: 'username'
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Roles',
      key: 'roles',
      dataIndex: 'roles',
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <button onClick={
            async () => {
              const apiDelete = `http://localhost:3222/users/${record.id}`
              const response = await axios.delete(apiDelete)
              console.log(response.data.statusCode)
              if(response.data.statusCode === 200) {
                alert(record.username + ' deleted')
                router.back()
              }else{
                alert('Something Wrong')
              }
            }
          }>Delete User</button>
        </Space>
      ),
    },
  ];

  const temp = datas.map((values) => {
    const data =
      {
        username: values.username,
        email: values.email,
        roles: values.roles[0].name,
        id: values.id
      }
    return data
  })

    return(
        <>
            <Layout pageTitle='Contest' />
            <div className='flex'>
                <Sidebar title="Contest" />
                <div className='w-full'>
                    <div className='h-screen mx-10'>
                      <Table columns={columns} dataSource={temp} />
                    </div>
                </div>
            </div>
        </>
    )
}