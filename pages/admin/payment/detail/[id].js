import axios from "axios"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import Layout from "../../../../components/Layout/Layout"
import Image from 'next/image'
import jwtDecode from 'jwt-decode'

export default function DetailTransaction () {

    const [datas, setDatas] = useState([])
    const [price, setPrice] = useState([])

    const router = useRouter()

    const getDataPayment = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/transactions/event-payment/detail/${id}`)
            .then((response) => {
                setDatas(response.data.data)
                const price = response.data.data.totalPayment
                const tempPrice = price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                setPrice(tempPrice)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    const handleBack = () => {
        try{
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            router.push(`/admin/payment/contestor/${decode.sub}`)
        }catch(err){
            console.log("Error : ", err)
        }
    }

    useEffect(() => {
        getDataPayment()
    }, [])

    return (
        <>
        <Layout pageTitle="Detail Payment" />
        <div className='flex w-screen h-screen'>
            <div className='flex items-center justify-center w-5/6 h-full'>
                <div className='shadow-2xl'>
                    <Image src={`http://localhost:3222/files/${datas?.proofOfPayment}`} width={400} height={500} />
                </div>
            </div>
            <div className='w-2/6 h-full pt-20'>
                <h1 className="text-base">Title Contest</h1>
                <p>{datas.eventTitle}</p>
                <h1 className="text-base">Username Creator</h1>
                <p>{datas.username}</p>
                <h1 className="text-base">Total Payment</h1>
                <p>Rp{price},00</p>
                <div onClick={handleBack} className="w-1/4 py-1 mt-10 text-center text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-md cursor-pointer hover:bg-teal-500 hover:text-white">                
                    <span>Back</span>
                </div>
            </div>
        </div>
        </>
    )
}