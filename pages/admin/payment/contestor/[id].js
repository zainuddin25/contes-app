import Sidebar from "../../../../components/admin/sidebar/sidebar";
import Layout from "../../../../components/Layout/Layout";
import { useEffect, useState } from "react";
import Link from 'next/link'
import axios from 'axios'
import { Table, Modal } from "antd";
import Image from 'next/image'

export default function PaymentContestor() {

    const [datas, setDatas] = useState([])
    const [modal1Visible, setModal1Visible] = useState(false);

    const getTransactionContestor = async () => {
        try{
            await axios.get(`http://localhost:3222/events/payment-status`)
            .then((response) => {
                setDatas(response.data.data)
                console.log(response)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    useEffect(() => {
        getTransactionContestor()
    }, [])

    const columns = [
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username'
        },
        {
            title: 'Title Contest',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Price',
            key: 'price',
            dataIndex: 'price',
        },
        {
            title: 'Action',
            key: 'action',
            render: (_, record) => (
                <Link href={`/admin/payment/detail/${record.id}`} >
                    <a className="text-black">Detail Transactions</a>
                </Link>
            )
        },
    ];
    
    const temp = datas.map((values) => {
        const tempPrize = values.competitionPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        const data =
        {
            username: values?.user?.username,
            title: values.title,
            price: 'Rp' + tempPrize + ',00',
            id: values.id
        }
        return data
    })

    return(
        <>
        <Layout pageTitle='Payment Contestor' />
        <div className='flex'>
            <Sidebar title='Payment Contestor' />
            <div className='w-full'>
                <div className='h-screen mx-10'>
                    <Table columns={columns} dataSource={temp} />
                </div>
            </div>
        </div>
        </>
    )
}