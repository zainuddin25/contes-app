import { UploadOutlined } from '@ant-design/icons';
import Layout from "../../../../../components/Layout/Layout";
import { Button, Form, Upload, message } from 'antd';
import axios from "axios"
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import jwtDecode from 'jwt-decode';

export default function Pay () {
    const [fileList, setFileList] = useState([])
    const [path, setPath] = useState([])
    const [payment, setPayment] = useState([])
    const [price, setPrice] = useState([])

    const router = useRouter()

    const handlerUploadPayment = async (args) => {
        try{
            const token = localStorage.getItem('token')
            const formData = new FormData()
            formData.append("file", args.file)
            await axios.post('http://localhost:3222/files/contestan-payment', formData, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                setFileList([
                    {
                        path: response.data.data,
                        status: 'done',
                        name: 'Upload Success'
                    }
                ])
                setPath(response.data.data)
                if(response.status === 201) {
                    message.success('Image Uploaded')
                }
            })
        } catch (err) {
            console.log("Wop, Error : ", err)
        }
    }

    const submitPayment = async () => {
        try{
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            const { id } = router.query
            await axios.post(`http://localhost:3222/transactions/contestan-payment/${id}`, {
                proofOfPayment: path
            }, {
                headers : {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                if(response.status === 201) {
                    message.success('Payment Success')
                    router.push(`/admin/payment/contestan/${decode.sub}`)
                }
            })
        }catch(err){
            console.log("Error : ", err)
        }
    } 

    const getDataWinner = async () => {
        try {
            const { id } = router.query
            await axios.get(`http://localhost:3222/contestans/winner/contestan/${id}`)
            .then((response) => {
                setPayment(response.data.data)
                const price = response.data.data.event.competitionPrice
                const tempPrice = price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                setPrice(tempPrice)
            })
        } catch (err) {
            console.log(err)
        }
    }

    // const handlerPayment = async () => {
    //     try{
    //         await axios.post(`http://`)
    //     }catch(err){
    //         message.error('Ups, Something Wrong')
    //         console.log("Error : ", err)
    //     }
    // }

    const handleCancelUpload = () => {
        try{
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            router.push(`/admin/payment/contestan/${decode.sub}`)
        }catch(err){
            console.log(err)
        }
    }

    useEffect(() => {
        getDataWinner()
    }, [])

    return (
        <>
        <Layout pageTitle="Payment" />
        <div className="w-screen h-screen bg-slate-50">
            <div className="w-2/4 h-full mx-auto shadow-lg bg-white">
                <h1 className="pt-8 text-2xl text-center">Payment Contestan</h1>
                <div className='w-3/4 mx-auto mb-4'>
                    <h1 className='font-semibold'>Username : <span className='font-normal'>{payment?.user?.username}</span></h1>
                    <h1 className='font-semibold'>Account Number : <span className='font-normal'>{payment?.user?.accountNumber}</span></h1>
                    <h1 className='font-semibold'>Title Event : <span className='font-normal'>{payment?.event?.title}</span></h1>
                    <h1 className='font-semibold'>Total Payment : <span className='font-normal'>Rp{price},00</span></h1>
                </div>
                <div className="w-3/4 h-auto mx-auto">
                    <Form name="basic" onFinish={submitPayment} autoComplete="off" >
                        <div className='flex items-center justify-center w-2/4 mx-auto mt-4 mb-6'>
                            <Upload customRequest={handlerUploadPayment} fileList={fileList}>
                                <Button style={{background: "transparent"}} icon={<UploadOutlined />}>Click to Upload Your Proof of Payment</Button>
                            </Upload>
                        </div>

                        <Form.Item >
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                            <Button onClick={handleCancelUpload} style={{marginLeft: "15px"}}>
                                Cancel
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
                {/* <Modal title={'Thanks ' + datas.username} visible={isModalVisible} footer={[
                    <Button key="submit" type="primary" onClick={handleOk}>
                        Next
                    </Button>,
                ]}>
                    <div className='w-full h-full'>
                        <h1 className='text-4xl text-center text-teal-500'>Payment Success!</h1>
                        <div className="flex flex-col items-center justify-center w-2/4 mx-auto">
                            <i className="fa-solid fa-check border-2 border-teal-500 text-teal-500 rounded-full text-3xl py-[13px] px-[18px]"></i>
                        </div>
                        <div className='w-3/4 mx-auto mt-8'>
                            <div className='flex justify-between'>
                                <p>Title Contest :</p>
                                <p>{datas.eventTitle}</p>
                            </div>
                            <div className='flex justify-between'>
                                <p>Total Payment :</p>
                                <p>{datas.totalPayment}</p>
                            </div>
                            <div className='flex justify-between'>
                                <p>From :</p>
                                <p>{datas.username}</p>
                            </div>
                            <hr />
                            <div className='mt-2 text-center'>
                                <p>ID Transactions</p>
                                <p>{datas.id}</p>
                            </div>
                        </div>
                    </div>
                </Modal> */}
            </div>
        </div>
        </>
    )
}