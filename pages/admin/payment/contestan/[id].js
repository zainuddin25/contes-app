import Sidebar from "../../../../components/admin/sidebar/sidebar";
import Layout from "../../../../components/Layout/Layout";
import { Table } from "antd";
import Link from 'next/link'
import axios from "axios";
import { useEffect, useState } from "react";

export default function PaymentContestan() {

    const [dataWinners, setDataWinners] = useState([])

    const getWinner = async () => {
        try{
            const token = localStorage.getItem('token')
            await axios.get('http://localhost:3222/contestans/winner/contestan', {
                headers : {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                setDataWinners(response.data.data)
            })
        }catch(err){
            console.log(err)
        }
    }

    useEffect(() => {
        getWinner()
    }, [])

    const columns = [
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username'
        },
        {
            title: 'Status Contestan',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Account Number',
            dataIndex: 'rek',
            key: 'rek',
        },
        {
            title: 'Pay',
            key: 'pay',
            render: (_, record) => (
                <Link href={`/admin/payment/contestan/pay/${record.id}`}>
                    <a className="text-black">Pay</a>
                </Link>
            )
        },
    ];
    
    const temp = dataWinners.map((values) => {
        const data =
          {
            username: values?.user?.username,
            status: values?.status,
            rek: values?.user?.accountNumber,
            id: values?.id
          }
        return data
    })

    return(
        <>
        <Layout pageTitle='Payment Contestan' />
        <div className='flex'>
            <Sidebar title='Payment Contestan' />
            <div className='w-full'>
                <div className='h-screen mx-10'>
                    <Table columns={columns} dataSource={temp} />
                </div>
            </div>
        </div>
        </>
    )
}