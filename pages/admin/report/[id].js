import Layout from '../../../components/Layout/Layout'
import HeaderAdmin from '../../../components/admin/header/headeradmin'
import Sidebar from '../../../components/admin/sidebar/sidebar'
import { message } from 'antd'
import axios from 'axios'
import React from 'react';
import Link from 'next/link'

export default function Report () {

    return (
        <>
        <Layout pageTitle="Dashboard" />
        <div className="flex">
            <Sidebar title="Dashboard Admin" />
            <div className="w-full">
                <HeaderAdmin />
                {/*  */}
                <div className="mb-4">
                    <h1 className="mx-6 text-2xl font-semibold">Report</h1>
                    <div className="grid-cols-3 gap-4 mx-6 lg:grid">
                        <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                            <p className="text-md text-slate-500">Export Data To Excel</p>
                            <div className='flex justify-between'>
                                <h1 className="text-2xl">Data Users</h1>
                                <Link href='http://localhost:3222/users/download/excel'>
                                    <a target="_blank" className='text-slate-900 hover:text-slate-900'> 
                                        <i class="fa-solid fa-download pr-6 text-xl pt-[6px] cursor-pointer"></i>
                                    </a>
                                </Link>
                            </div>
                        </div>
                        <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                            <p className="text-md text-slate-500">Export Data To Excel</p>
                            <div className='flex justify-between'>
                                <h1 className="text-2xl">Data Events</h1>
                                <Link href='http://localhost:3222/events/download-excel'>
                                    <a target="_blank" className='text-slate-900 hover:text-slate-900'> 
                                        <i class="fa-solid fa-download pr-6 text-xl pt-[6px] cursor-pointer"></i>
                                    </a>
                                </Link>
                            </div>
                        </div>
                        <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                            <p className="text-md text-slate-500">Export Data To Excel</p>
                            <div className='flex justify-between'>
                                <h1 className="text-2xl">Data Winners</h1>
                                <Link href='http://localhost:3222/contestans/download/detail/excel'>
                                    <a target="_blank" className='text-slate-900 hover:text-slate-900'> 
                                        <i class="fa-solid fa-download pr-6 text-xl pt-[6px] cursor-pointer"></i>
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}