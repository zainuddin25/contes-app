import { ExclamationCircleOutlined } from '@ant-design/icons';
import Sidebar from '../../../components/admin/sidebar/sidebar' 
import HeaderAdmin from '../../../components/admin/header/headeradmin'
import Layout from '../../../components/Layout/Layout'
import React, { useEffect, useState } from 'react';
import axios from 'axios'
import Link from 'next/link'
import { useRouter } from 'next/router';
import useAuthenicatedPage from '../../../helper/Authentication'
import { Button, Modal } from 'antd'
const { confirm } = Modal

export default function Contest() {

    useAuthenicatedPage()
  
    const [datas, setDatas] = useState([])
    // const [status, setStatus] = useState([])
    const [idContest, setIdContest] = useState([])
    
    const router = useRouter()

    useEffect(() => {
        getDataEvents()
    },[])

    const getDataEvents = async() => {
        try{
          const token = localStorage.getItem('token')
          await axios.get(`http://localhost:3222/events`, {
            headers: {
              'Authorization' : `Bearer ${token}`
            }
          })
          .then((response) => {
            setDatas(response.data.data)
          })
        } catch(err) {
          console.log(err)
        }
    }


    const showConfirm = () => {
        confirm({
          title: 'Are you sure you want to change the status of this contest?',
          icon: <ExclamationCircleOutlined />,
          content: 'Make sure this contest has followed the rules already in JustContes',
      
          onOk() {
            handleOk()
          },
      
          onCancel() {
            console.log('Cancel');
          },
        });
    };

    return(
        <>
            <Layout pageTitle='Contest' />
            <div className='flex'>
                <Sidebar title="Contest" />
                <div className='w-full'>
                    <HeaderAdmin />
                    <div className="flex flex-col px-4 mx-auto lg:grid lg:grid-cols-4 lg:gap-4">
                    {datas?.map((values) => {
                        return (
                        <>
                            <div className="flex justify-center mt-8 lg:mt-0">
                                <div className="w-full rounded-lg shadow-lg">
                                    <div className="flex justify-center w-full rounded-lg">
                                        <img className='flex items-center justify-center' src={`http://localhost:3222/files/${values?.thumbnail}`} width={300} height={300}/>
                                    </div>
                                    <div>
                                        <div className='px-4 pt-2'>
                                          <h1>Title : {values?.title}</h1>
                                          <h1>Contestor : {values?.user?.username}</h1>
                                          <h1>Status : {values?.status}</h1>
                                        </div>
                                        <div className='w-3/4 mx-auto mt-4'>
                                            <p className='py-2 text-center text-teal-500 border border-teal-500 rounded-lg cursor-pointer' onClick={() => {router.push(`/admin/editstatus/${values.id}`)}}>Edit Status</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                        )
                    })}
                    </div>
                </div>
            </div>
        </>
    )
}