import Sidebar from "../../../components/admin/sidebar/sidebar"
import HeaderAdmin from "../../../components/admin/header/headeradmin"
import Layout from "../../../components/Layout/Layout"
import React, { useEffect, useState } from 'react';
import axios from "axios";
import { useRouter } from "next/router";
import useAuthenicatedPage from "../../../helper/Authentication";

export default function Dashboard() {
  const [admin, setAdmin] = useState([])
  const [contestor, setContestor] = useState([])
  const [contestan, setContestan] = useState([])
  const [events, setEvents] = useState([])
  const [eventsReady, setEventsReady] = useState([])
  const [eventPending, setEventPending] = useState([])
  const [posts, setPosts] = useState([])
  const totalAccount = admin + contestor + contestan;

  useAuthenicatedPage()

  const router = useRouter()
  
  const getUserData = async () => {
    const apiGetContestor = 'http://localhost:3222/users/role/contestor'
    const apiGetContestan = 'http://localhost:3222/users/role/contestan'
    const apiGetAdmin = 'http://localhost:3222/users/role/admin'
    if(window !== undefined) {
      const token = localStorage.getItem('token')
      const responseContestan = await axios.get(apiGetContestan, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      const responseContestor = await axios.get(apiGetContestor, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      const responseAdmin = await axios.get(apiGetAdmin, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      setContestor(responseContestor.data.count)
      setContestan(responseContestan.data.count)
      setAdmin(responseAdmin.data.count)
      if(token === undefined) {
        router.push('./')
      }
    }else{
      router.push('/Register/Login/Login')
    }
  }

  const getEventsData = async () => {
    try{
      const token = localStorage.getItem('token')
      await axios.get(`http://localhost:3222/events`, {
        headers: {
          'Authorization' : `Bearer ${token}`
        }
      })
      .then((response) => {
        setEvents(response.data.count)
      })
    }catch(err) {
      console.log("Error : ", err)
    }
  }

  const getEventsReady = async () => {
    try{
      await axios.get(`http://localhost:3222/events/ready`)
      .then((response) => {
        setEventsReady(response.data.count)
      })
    }catch(err){
      console.log("Error : ", err)
    }
  }

  const getPosts = async () => {
    try{
      await axios.get('http://localhost:3222/posts')
      .then((response) => {
        setPosts(response.data.count)
      })
    }catch(err){
      console.log("Error : ", err)
    }
  } 

  const getPendingEvents = async () => {
    try{
      const token = localStorage.getItem('token')
      await axios.get(`http://localhost:3222/events/pending`, {
        headers: {
          'Authorization' : `Bearer ${token}`
        }
      })
      .then((response) => {
        // console.log(response)
        setEventPending(response.data.count)
      })
    }catch(err){
      console.log("Error : ", err)
    }
  }

  useEffect(() => {
    getUserData()
    getEventsData()
    getEventsReady()
    getPendingEvents()
    getPosts()
  },[])

  return (
      <>
      <Layout pageTitle="Dashboard" />
      <div className="flex">
          <Sidebar title="Dashboard Admin" />
          <div className="w-full">
              <HeaderAdmin />
              {/*  */}
              <div className="mb-4">
                <h1 className="mx-6 text-2xl font-semibold">Account</h1>
                <div className="grid-cols-3 gap-4 mx-6 lg:grid">
                  <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                    <p className="text-md text-slate-500">Contestor Account</p>
                    <h1 className="text-2xl">{contestor} Account</h1>
                  </div>
                  <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                    <p className="text-md text-slate-500">Contestan Account</p>
                    <h1 className="text-2xl">{contestan} Account</h1>
                  </div>
                  <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                    <p className="text-md text-slate-500">Admin Account</p>
                    <h1 className="text-2xl">{admin} Account</h1>
                  </div>
                  <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                    <p className="text-md text-slate-500">Total Account</p>
                    <h1 className="text-2xl">{totalAccount} Account</h1>
                  </div>
                </div>
              </div>
              <div className="mt-4">
                <h1 className="mx-6 text-2xl font-semibold">Contest</h1>
                <div className="grid-cols-3 gap-4 mx-6 lg:grid">
                  <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                    <p className="text-md text-slate-500">Ready Events</p>
                    <h1 className="text-2xl">{eventsReady} Events</h1>
                  </div>
                  <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                    <p className="text-md text-slate-500">Panding Events</p>
                    <h1 className="text-2xl">{eventPending} Events</h1>
                  </div>
                  <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                    <p className="text-md text-slate-500">Total Contest</p>
                    <h1 className="text-2xl">{events} Events</h1>
                  </div>
                </div>
              </div>
              <div className="mt-4">
                <h1 className="mx-6 text-2xl font-semibold">Posts</h1>
                <div className="grid-cols-3 gap-4 mx-6 lg:grid">
                  <div className="pt-3 pl-6 border rounded-md border-slate-100 bg-slate-50">
                    <p className="text-md text-slate-500">Total Posts</p>
                    <h1 className="text-2xl">{posts} Posts</h1>
                  </div>
                </div>
              </div>
              {/*  */}
          </div>
      </div>
      </>
  )
}