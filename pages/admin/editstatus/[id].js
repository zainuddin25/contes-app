import { message } from "antd"
import axios from "axios"
import jwtDecode from "jwt-decode"
import Image from "next/image"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import useAuthenicatedPage from "../../../helper/Authentication"

export default function EditStatus () {

    const router = useRouter()
    const [datas, setDatas] = useState([])

    useAuthenicatedPage()
    
    const getDataEvents = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/transactions/event-payment/detail/${id}`)
            .then((response) => {
                setDatas(response.data.data)
                console.log(response.data.data)
            })
        }catch(err){
            console.log("Error : " ,err)
        }
    }

    const handleEdit = async () => {
        try{
            const { id } = router.query
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            await axios.put(`http://localhost:3222/events/update-status/${id}`,{}, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
              if(response.status === 200) {
                message.success("Update Success")
                router.push(`/admin/contest/${decode.sub}`)
              }
            })
        } catch(err) {
            console.log(err)
        }
    }

    useEffect(() => {
        getDataEvents()
    }, [])

    return (
        <>
        <div className="flex items-center justify-center w-screen h-screen">
            <div className="mx-auto text-center">
                <Image src={`http://localhost:3222/files/${datas?.proofOfPayment}`} width={400} height={400} />
                <h1 className="text-2xl uppercase">{datas.title}</h1>
                <p>Status : {datas.status}</p>
                <div className="flex justify-center mt-2">
                    <p onClick={handleEdit} className="py-2 mr-2 text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-sm cursor-pointer w-36 hover:bg-teal-500 hover:text-white">Edit Status</p>
                    <p onClick={() => router.back()} className="py-2 ml-2 text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-sm cursor-pointer w-36 m hover:bg-teal-500 hover:text-white">Cancel</p>
                </div>
            </div>
        </div>
        </>
    )
}