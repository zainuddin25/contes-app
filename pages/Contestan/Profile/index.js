import { ExclamationCircleOutlined } from '@ant-design/icons';
import HeaderContestan from "../../../components/Header/headercontestan/headercontestan"
import Layout from '../../../components/Layout/Layout'
import { Card, Button, Modal, message } from 'antd'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Footer from '../../../components/Footer/Footer'
import useAuthenicatedPage from '../../../helper/Authentication'
import Image from 'next/image'
const { confirm } = Modal;

export default function Profile() {

    useAuthenicatedPage()
                
    const router = useRouter()

    const [username, setUsername] = useState([])
    const [idUser, setIdUser] = useState([])
    const [post, setPost] = useState()
    const [description, setDescription] = useState('')
    const [data, setData] = useState([])
    const [accountNumber, setAccountNumber] = useState([])
    const [photoProfile, setPhotoProfile] = useState([])
    const [activeTabKey2, setActiveTabKey2] = useState('upload');
    const [like, setLike] = useState(0)
    const [idPost, setIdPost] = useState([])

    useEffect(() => {
        getPost()
        profile()
        getPathName()
    }, [])

    const profile = async () => {
        try{
            const { id } = router.query
            setIdUser(id)
            const apiGetProfile = `http://localhost:3222/users/${id}`
            const response = await axios.get(apiGetProfile)
            const data = response.data.data;
            setUsername(data.username)
            setDescription(data.about)
            setAccountNumber(data.accountNumber)
        } catch (err) {
            message.error("Ups, something wrong")
        }
    }

    const getPathName = async () => {
        try{
            const { id } = router.query
            const apiGetPath = `http://localhost:3222/users/${id}`
            const response = await axios.get(apiGetPath)
            const data = response.data.data
            setPhotoProfile(data.photoProfile)
            setData(data)
        } catch (err) {
            message.error("Ups, something wrong")
        }
    }

    const getPost = async () => {
        try{
            const { id } = router.query
            const apiGetPost = `http://localhost:3222/posts/user/${id}`
            const response = await axios.get(apiGetPost)
            setPost(response.data.data)
        } catch (err) {
            message.error("Ups, something wrong")
        }
    }

    const handleDelete = async () => {
        try{
            const { id } = router.query
            const token = localStorage.getItem('token')
            await axios.delete(`http://localhost:3222/posts/${idPost}`, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                if(response.status === 200) {
                    message.success('Delete Success')
                    router.push(`/contestan/deleted/${id}`)
                    setIdPost([])
                }
            })
        }catch(err){
            message.error('Ups, Something Wrong, Please try again')
            console.log("Error : ", err)
        }
    }

    const showDeleteConfirm = () => {
        confirm({
            title: 'Are you sure delete this post?',
            icon: <ExclamationCircleOutlined />,
            content: 'If you delete this post, the post will be deleted from your profile and global posts',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',

            onOk() {
                handleDelete()
            },

            onCancel() {
                message.info('Cancel Delete')
            },
        });
    };

    const tabListNoTitle = [
        {
            key: 'about',
            tab: 'About',
        },
        {
            key: 'upload',
            tab: 'Upload',
        },
    ];

    const onTab2Change = (key) => {
        setActiveTabKey2(key);
    };

    const contentListNoTitle = {
        about: 
        <>
            <div className="flex flex-col">
                <div className="w-full">
                    <h1 className="text-2xl font-bold">Description</h1>
                    <p>{description}</p>
                    <h1 className='text-2xl font-bold'>Account Number</h1>
                    <p>{accountNumber}</p>
                </div>
            </div>
        </>,
        upload: 
        <>
        <div className="flex flex-col px-8 py-10 mx-auto rounded-sm lg:grid lg:grid-cols-4 lg:gap-4" style={{
            boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.1)'
        }}>
        {post?.map((values) => {
            return (
                <>
                <div className="relative flex justify-center mt-8 lg:mt-0" key={values?.id}>
                    <div className="w-full rounded-lg shadow-md">
                        <div className="flex justify-center w-full rounded-lg">
                            <Image className='flex items-center justify-center' style={{borderTopRightRadius: "5px", borderTopLeftRadius: "5px"}} src={`http://localhost:3222/files/${values?.linkPath}`} width={300} height={300}/>
                        </div>
                        <div className="flex justify-between">
                            <p className="text-lg"><i className="pr-3 mt-4 ml-6 text-lg fa-solid fa-heart text-slate-500"></i>{like}</p>
                            <p className='mr-6 text-lg'><i className="pt-4 pr-3 text-lg fa-solid fa-comment text-slate-500"></i>{like}</p>
                        </div>
                    </div>
                    <div className='absolute top-0 right-0'>
                        <p onClick={() => {setIdPost(values?.id)}}><i onClick={showDeleteConfirm} className="pt-2 pr-2 ease-in-out cursor-pointer hover:opacity-90 text-md fa-solid fa-trash-can opacity-30"></i></p>
                    </div>
                </div>
                </>
            )
        })}
        </div>
        </>,
    };
 
    return (
        <>
            <Layout pageTitle="Profile" />  
            <HeaderContestan />
            <div className='container mx-auto'>
                <div className="relative flex py-3 mx-10 bg-no-repeat bg-cover rounded-b-2xl h-52"
                    style={{
                        backgroundImage: "url(" + `http://localhost:3222/files/${data?.backgroundProfile}` + ")"
                    }}
                >
                        <div className="border-4 left-4 w-auto px-[2px] bg-white border-white rounded-full absolute top-44">
                            <div className="relative">
                                <Image src={`http://localhost:3222/files/${data?.photoProfile}`} width={110} height={110} style={{borderRadius: "100%"}}/>
                                <Link href={`/contestan/editprofile/${idUser}`}>
                                    <div className="absolute top-20 left-[80px] cursor-pointer">
                                        <i className="px-2 py-2 text-black bg-white border rounded-full fa-solid fa-pen"></i>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        <div className="absolute top-[166px] bg-white bg-opacity-50 hover:bg-opacity-70 ease-in-out duration-200 rounded-full right-3">
                            <div>
                                <Link href={`/contestan/editbackground/${idUser}`}>
                                    <a><i className="px-2 py-2 text-black fa-solid fa-camera"></i></a>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="flex flex-row w-full pt-1 pl-48">
                        <div className="w-2/4">

                            <span className="text-lg font-bold">{username}</span> <br />
                            <span className="text-sm"><i className="fa-solid fa-trophy"></i> 10 Winner</span>
                        </div>
                        <div className="w-2/4 pr-12 mt-1 text-right">
                            <Link href={`/contestan/editprofile/${idUser}`}>
                                <Button>
                                    Edit Profile
                                </Button>
                            </Link>
                            <Link href={`/contestan/uploadimage/${idUser}`}>
                                <Button type="primary" htmlType="submit" style={{marginTop: "10px", marginLeft: "10px"}}>
                                    Add
                                </Button>
                            </Link>
                        </div>
                    </div>
                    <div className="container mx-auto">
                        <div className="mx-4 mt-10">
                            <Card bordered={false} style={{ width: '100%' }} tabList={tabListNoTitle} activeTabKey={activeTabKey2} onTabChange={(key) => { onTab2Change(key) }}>{contentListNoTitle[activeTabKey2]}</Card>
                        </div>
                    </div>
                </div>
                <Footer />
            </>
    )
}