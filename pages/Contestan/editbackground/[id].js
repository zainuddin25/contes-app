import Layout from "../../../components/Layout/Layout";
import { UploadOutlined } from '@ant-design/icons';
import { Upload, Button, message } from "antd";
import { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import useAuthenicatedPage from "../../../helper/Authentication";
import Image from 'next/image'

export default function UploadBackground() {
    const [fileList, setFileList] = useState()
    const [path, setPath] = useState()
    
    const router = useRouter()
    const { id } = router.query
    const token = localStorage.getItem('token')

    useAuthenicatedPage()

    const uploadHandler = async (args) => {
        try{
            const formData = new FormData()
            formData.append("file", args.file)
            const result = await axios.post('http://localhost:3222/files/background-profile', formData, {
                headers: {
                    'Content-Type' : 'multipart/form-data',
                    'Authorization' : `Bearer ${token}`,
                }
            })
            .then((res) => {
                setFileList([
                    {
                        path: res.data.data,
                        status: 'done',
                        name: 'Upload Success'
                    }
                ])
                setPath(res.data)
                if (res.data.message === 'success') {
                    message.success(`file uploaded successfully`);
                }
            })
        } catch (err) {
            console.log('Error Brok : ', err )
        }
    }

    const handleSubmitBackground = async () => {
        const apiUploadImage = `http://localhost:3222/users/profile/${id}`
        const response = await axios.put(apiUploadImage, {
            backgroundProfile: path.data,
        }, {
            headers: {
                'Authorization' : `Bearer ${token}`
            }
        })
        if(response.status === 200) {
            message.success('Edit Success')
            router.push(`/contestan/profile/${id}`)
        }
    }

    const handleCancelSubmit = () => {
        router.push(`/contestan/profile/${id}`)
    }

    return (
        <>
            <Layout pageTitle='Upload Image' />
            <div className="w-screen h-screen">
                <div className="relative flex flex-col items-center justify-center w-full shadow-lg bg-slate-50 h-3/4">
                    <div className="mb-4">
                        <Image src={`http://localhost:3222/files/${path?.data}`} width={600} height={150} />
                    </div>
                    <Upload customRequest={(args) => uploadHandler(args)} fileList={fileList}>
                        <Button style={{background: "transparent"}} icon={<UploadOutlined />}>Click to Upload Background</Button>
                    </Upload>
                    <p className="absolute flex items-center w-auto top-5 left-5 lg:top-10 lg:left-20"><div className="w-16 border border-teal-500 rounded-full"></div><span className="ml-2">Upload Background Profile Image</span></p>
                    <p className="absolute px-[15px] py-2 border rounded-full top-3 cursor-pointer right-5 lg:top-7 ease-in-out duration-200 lg:right-20 border-teal-500 text-teal-500 hover:bg-teal-500 hover:text-white"><i class="fa-solid fa-xmark"></i></p>
                </div>
                <div className="relative flex justify-center w-full h-1/4">
                    <div className="absolute flex bottom-4">
                        <h1 onClick={handleCancelSubmit} className="px-10 py-2 mr-2 text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-lg cursor-pointer hover:bg-teal-500 hover:text-white">Cancel</h1>
                        <h1 onClick={handleSubmitBackground} className="px-10 py-2 ml-2 text-white duration-200 ease-in-out bg-teal-500 border rounded-lg cursor-pointer hover:bg-teal-700">Submit</h1>
                    </div>
                </div>
            </div>
        </>
    )
}