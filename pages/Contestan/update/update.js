import { Checkbox, Button, Alert, message } from 'antd';
import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import Layout from '../../../components/Layout/Layout';
import useAuthenicatedPage from '../../../helper/Authentication';
import bgImage from '../../../public/bgImage.jpg'

export default function Update() {
    const [score, setScore] = useState(0)
    const router = useRouter()
    const { id } = router.query

    useAuthenicatedPage()

    const onChangeOne = (e) => {
        if(e.target.checked === true) {
            setScore(score + 10)
        }else{
            setScore(score - 10)
        }
    };
    const onChangeTwo = (e) => {
        if(e.target.checked === true) {
            setScore(score + 10)
        }else{
            setScore(score - 10)
        }
    };
    const onChangeThree = (e) => {
        if(e.target.checked === true) {
            setScore(score + 10)
        }else{
            setScore(score - 10)
        }
    };

    if(score < 0) {
        setScore(0)
    }

    const handleClick = async () => {
        if(score > 29) {
            message.success('😍 Congratulations now you are a Contestor');
            const apiUpdateRole = `http://localhost:3222/users/update-role/${id}`
            const response = await axios.put(apiUpdateRole)
            if(response.data.message === "success") {
                router.push('/Register/Login/Login')
            } 
        } else {
            message.error("🥱 Sorry, you still don't meet our requirements");
            router.push(`/contestan/profile/${id}`)
        }
    }

    return(
        <>
            <Layout pageTitle="update to contestor" />
            <div className="flex justify-center w-full h-screen" style={{
                backgroundImage: `url(${bgImage.src})`,
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'
            }}>
                <div className="w-3/4 my-10 bg-white rounded-lg lg:w-1/3 opacity-90 backdrop-blur-sm">
                    <div className="px-8 py-10">
                        <h1 className="text-2xl font-bold text-center uppercase">Welcome To Contestor</h1>
                        <p className="w-full mx-auto text-center">
                            Please fill in the following data for your requirements to enter the contestor, try to fill it as it is because this data is very important, thank you
                        </p>
                        <div className="w-full mx-auto">
                            <p className="my-2">1. I agree with all the terms</p>
                            <Checkbox onChange={onChangeOne}>I agree</Checkbox>
                            <p className='my-2'>2. I agree if I have a winner in a contest then I will receive 5% tax</p>
                            <Checkbox onChange={onChangeTwo}>I agree</Checkbox>
                            <p className='my-2'>3. I agree if I cheat my account will be banned and can't be used again</p>
                            <Checkbox onChange={onChangeThree}>I agree</Checkbox>
                        </div>
                        <div className="w-auto mx-auto mt-8">
                            <p className="py-2 text-center text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-md cursor-pointer hover:bg-teal-500 hover:text-white" onClick={handleClick}>Update</p>
                        </div>
                    </div>
                    
                </div>
            </div>

        </>
    )
}