import { useRouter } from "next/router"
import { useEffect, useState } from "react"

export default function Deleted () {

    const router = useRouter()
    const { id } = router.query 
    const [second, setSecond] = useState(3)

    useEffect(() => {
        setTimeout(() => {
            setSecond(second - 1)
            if(second === 0) {
                router.push(`/contestan/profile/${id}`)
                setSecond(0)
            }
        }, 1000);
    })

    return (
        <>
        <div className="flex items-center justify-center w-full h-screen">
            <div className="w-2/4 border rounded-md shadow-md">
                <div className='w-full text-center'>
                    <h1 className="pt-5 text-2xl font-bold">Delete Successful</h1>
                    <p>Wait 3 seconds you will be directed to your profile page</p>
                    <p className="py-10 text-4xl font-bold">{second}</p>
                </div>
            </div>
        </div>
        </>
    )
}