import Layout from "../../../components/Layout/Layout"
import { Upload, Button, Form, Input, message } from 'antd'
import { UploadOutlined } from '@ant-design/icons';
import { useRouter } from "next/router";
import axios from "axios";
import React, { useState } from "react";
import useAuthenicatedPage from "../../../helper/Authentication";

export default function EditProfile() {

    const [username, setUsername] = useState([])
    const [description, setDescription] = useState([])
    const [path, setPath] = useState([])
    const [fileList, setFileList] = useState([])
    const [rek, setRek] = useState([])
    const [form] = Form.useForm();

    const router = useRouter()
    const { id } = router.query
    const token = localStorage.getItem('token')

    useAuthenicatedPage()

    const onFinishFailed = () => {
        console.log('failed')
    }

    const getDataUser = async () => {
        const apiGetUser = `http://localhost:3222/users/${id}`
        const response = await axios.get(apiGetUser)
        const data = response.data.data
        setUsername(data.username)
        setDescription(data.description)
        setRek(data.accountNumber)
    }

    const updateUser = async (values) => {
        try{
            const token = localStorage.getItem('token')
            const apiUpdate = `http://localhost:3222/users/profile/${id}`
            await axios.put(apiUpdate, {
                username: values.username,
                about: values.description,
                accountNumber: values.accountNumber
            },
            {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                if(response.status === 200) {
                    message.success('Edit Success')
                    router.push(`/contestan/profile/${id}`)
                }
            })  
        } catch (err) {
            message.error('Username Already Exists')
            console.log("Ada Error : ", err)
        }
    }

    const handlerImageUpload = async (args) => {
        try{
            const formData = new FormData()
            formData.append("file", args.file)
            await axios.post('http://localhost:3222/files/photo-profile', formData, {
                headers: {
                    'Authorization' : `Bearer ${token}`,
                }
            })
            .then((response) => {
                setFileList([
                    {
                        path: response.data.data,
                        status: 'done',
                        name: 'Upload Success'
                    }
                ])
                if(response.status === 201) {
                    message.success('Image Uploaded')
                }
                setPath(response.data.data)
            })
        } catch (err) {
            console.log("Wop, Error : ", err)
        }
    }

    const handleSaveImage = async () => {
        try{   
            const apiEditPhoto = `http://localhost:3222/users/profile/${id}`
            await axios.put(apiEditPhoto, {
                photoProfile: path,
            }, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                if(response.status === 200) {
                    message.success('Your Photo Profile Updated')
                }
            })
        } catch (err) {
            console.log("Error : ", err)
        }
    }

    const handleCancel = async () => {
        router.push(`/contestan/profile/${id}`)
    }

    React.useEffect(() => {
        getDataUser()
        form.setFieldsValue({
            username: username,
            description: description,
            accountNumber: rek
        });
    })

    console.log(path)


    return(
        <>
            <Layout pageTitle="Edit Profile" />
            <div className="flex flex-col h-screen lg:flex-row w-scree">
                <div className="relative flex flex-col items-center justify-center w-full shadow-lg lg:w-3/4 lg:h-full h-2/6">
                    <Upload customRequest={handlerImageUpload} fileList={fileList}>
                        <Button style={{background: "transparent"}} icon={<UploadOutlined />}>Click to Upload Your Image</Button>
                    </Upload>
                    <button onClick={handleSaveImage} className="absolute px-6 py-2 text-xs text-teal-500 border border-teal-500 rounded-sm lg:bottom-10 lg:px-10 bottom-4 hover:text-white hover:bg-teal-500">Save Image</button>
                    <p className="absolute flex items-center w-auto top-5 left-5 lg:top-10 lg:left-20"><span className="w-16 border border-teal-500 rounded-full"></span><span className="ml-2">Edit Profile</span></p>          
                    <p onClick={handleCancel} className="absolute px-[15px] py-2 border rounded-full top-3 cursor-pointer right-5 lg:top-7 ease-in-out duration-200 lg:right-20 border-teal-500 text-teal-500 hover:bg-teal-500 hover:text-white"><i className="fa-solid fa-xmark"></i></p>
                </div>
                <div className="w-full px-10 pt-6 h-4/6 lg:w-1/4 lg:h-full">
                    <Form form={form} onFinish={updateUser} onFinishFailed={onFinishFailed} autoComplete="off">
                        <h1>Username</h1>
                        <Form.Item name="username" rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <h1>Account Number</h1>
                        <Form.Item name="accountNumber" rules={[{ required: true }]}>
                            <Input />
                        </Form.Item>
                        <h1>Description</h1>
                        <Form.Item name="description">
                            <Input.TextArea style={{height: "180px"}} showCount maxLength={300} />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" style={{marginRight: "10px"}} htmlType="submit">
                                Submit
                            </Button>
                            <Button style={{marginLeft: "10px"}} onClick={handleCancel}>
                                Cancel
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </>
    )
}