import { Input, message, Button, Modal } from 'antd';
import photo from "../../../public/favicon.ico"
import Layout from '../../../components/Layout/Layout';
import Header from '../../../components/Header/headercontestor/headercontestor';
import Footer from "../../../components/Footer/Footer"
import React from 'react';
import Image from "next/image"
import Link from "next/link"
import axios from "axios"
import { useEffect, useState } from "react"
import { useRouter } from 'next/router';
import useAuthenicatedPage from '../../../helper/Authentication';

export default function ListContest() {

    const router = useRouter()

    const [events, setEvents] = useState([])
    const [dataEvents, setDataEvents] = useState([])
    const [modal1Visible, setModal1Visible] = useState(false)

    useAuthenicatedPage()

    const getEvents = async () => {
        try{
            await axios.get(`http://localhost:3222/events/ready`)
            .then((response) => {
                setEvents(response.data.data)
            })
        } catch (err) {
            message.error('Ups, something wrong')
            console.log('Error :',err)
        }
    }

    const info = () => {
        Modal.info({
          title: "Oops, the contest looks like it's over",
          content: (
            <div>
              <p>Please look for other ongoing contests</p>
            </div>
          ),
      
          onOk() {},
        });
    };

    useEffect(() => {
        getEvents()
    }, [])

    const handelSearch = async (values) => {
        const search = (values.target.value)
        try{
            await axios.get(`http://localhost:3222/events/search?title=${search}`)
            .then((response) => {
                setDataEvents(response.data.data)
            })
        }catch(err){
            console.log(err)
        }
    }

    const onChange = (e) => {
        console.log(e.target.value);
    };

    return(
        <>
            <Layout pageTitle="List Contest" />
            <Header />
            <div className="fixed z-40 w-screen px-4 pt-20 bg-white drop-shadow-sm">
                <div className="container flex justify-between mx-auto">
                    <h1 className="text-2xl font-bold uppercase lg:text-3xl">Contest List</h1>
                    <div className="w-2/4 lg:w-auto">
                        <Button onClick={() => setModal1Visible(true)}>
                            Search Contest
                        </Button>
                    </div>
                </div>
            </div>

            <div className="container z-0 px-4 py-24 mx-auto">
                <div className="mt-10 mx-7 lg:mx-20">
                    {events?.map((value) => {
                        const tempPrice = value.competitionPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                        const status = () => {
                            if(value.isActive === true) {
                                return (
                                    <Link href={`/joincontest/${value.id}`}>                                  
                                        <button className="w-2/4 py-2 mb-4 ml-1 text-white duration-200 ease-in-out bg-teal-500 border border-teal-500 rounded-md hover:bg-teal-600 hover:border-teal-600">
                                            Join Contest
                                        </button>
                                    </Link>
                                )
                            } else {
                                return (
                                    <button onClick={info} className="w-2/4 py-2 mb-4 ml-1 text-white duration-200 ease-in-out bg-teal-500 border border-teal-500 rounded-md hover:bg-teal-600 hover:border-teal-600">
                                        Join Contest
                                    </button>
                                )
                            }
                        }
                        return(
                        <>
                            <div className="flex">
                                <div className="container flex flex-col justify-center lg:flex-row">
                                <div className="w-auto py-4 mx-auto lg:px-6">
                                    <Image src={`http://localhost:3222/files/${value?.thumbnail}`} className='rounded-lg' width={215} height={215} />
                                </div>
                                <div className="w-full pl-10 lg:w-3/4 lg:pt-4">
                                        <h1 className="text-lg font-bold text-center lg:text-left">{value?.title}</h1>
                                        <p className="text-sm font-light text-center lg:text-left">{value?.description}</p>
                                        <div className="flex lg:w-2/4">
                                            <Link href={`/contestorfitur/contestordetail/${value.id}`}>
                                                <button className="w-2/4 py-2 mb-4 mr-1 text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-md hover:bg-teal-500 hover:text-white hover:border-white">
                                                    Detail Contest
                                                </button>
                                            </Link>
                                            {status()}
                                        </div>
                                    </div>
                                    <div className="hidden w-full px-4 lg:pt-4 lg:w-1/6 lg:block">
                                        <span className="font-semibold">Deadline :</span> <br/>
                                        <span>{value.deadline}</span> <br />
                                        <span className='font-semibold'>Price :</span> <br />
                                        <span>Rp{tempPrice},00</span> <br />
                                        <span className='font-semibold'>From :</span> <br />
                                        <span>{value.user.username}</span>
                                    </div>
                                </div>
                            </div>
                            <hr className="mb-10 mt-7" />
                        </>
                        )
                    })}
                </div>
                <Modal title="Search Contest" style={{ top: 20, }} visible={modal1Visible} onOk={() => setModal1Visible(false)} onCancel={() => setModal1Visible(false)}>
                    <Input placeholder="Search Contest" name="search" onChange={handelSearch} autoComplete="off" />
                    {dataEvents?.map((values) => {
                        return (
                            <>
                            <div className='flex justify-between mx-1 mt-4' key={values}>
                                <h1>{values.title}</h1>
                                <div>
                                    <Link href={`/contestanfitur/contestandetail/${values.id}`}>
                                        <a>Detail Contest</a>
                                    </Link>
                                </div>
                            </div>
                            </>
                        )
                    })}
                </Modal>
            </div>
            <Footer />
        </>
    )
}