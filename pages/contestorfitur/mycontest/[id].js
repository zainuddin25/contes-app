import Layout from "../../../components/Layout/Layout"
import HeaderContestor from "../../../components/Header/headercontestor/headercontestor"
import Footer from "../../../components/Footer/Footer"
import axios from "axios"
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import useAuthenicatedPage from "../../../helper/Authentication"

export default function MyContest() {

    const [events, setEvents] = useState([])

    const router = useRouter()

    useAuthenicatedPage()

    const getMyEvents = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/events/contestan/event/${id}`)
            .then((response) => {
                setEvents(response.data.data)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    useEffect(() => {
        getMyEvents()
    },[])

    return(
        <>
        <Layout pageTitle="My Contest" />
        <HeaderContestor />
        <div className="fixed z-40 w-screen px-4 pt-20 bg-white drop-shadow-sm">
            <div className="container flex justify-between mx-auto">
                <h1 className="text-2xl font-bold uppercase lg:text-3xl">Contest List</h1>
                <div className="w-2/4 lg:w-1/4">
                </div>
            </div>
        </div>

        <div className="container z-0 px-4 py-24 mx-auto">
            <div className="mt-10 mx-7 lg:mx-20">
                {events?.map((value) => {
                    const tempPrice = value.competitionPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                    return(
                    <>
                        <div className="flex">
                            <div className="container flex flex-col justify-center lg:flex-row">
                            <div className="w-auto py-4 mx-auto lg:px-6">
                                <Image src={`http://localhost:3222/files/${value?.thumbnail}`} className='rounded-lg' width={215} height={215} />
                            </div>
                            <div className="w-full pl-10 lg:w-3/4 lg:pt-4">
                                    <h1 className="text-lg font-bold text-center lg:text-left">{value.title}</h1>
                                    <p className="text-sm font-light text-center lg:text-left">{value.description}</p>
                                    <div className="flex lg:w-2/4">
                                        <Link href={`/contestorfitur/contestordetail/${value.id}`}>
                                            <button className="w-2/4 py-2 mb-4 mr-1 text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-md hover:bg-teal-500 hover:text-white hover:border-white">
                                                View Contest
                                            </button>
                                        </Link>
                                    </div>
                                </div>
                                <div className="hidden w-full px-4 lg:pt-4 lg:w-1/6 lg:block">
                                    <span className="font-semibold">Deadline :</span> <br/>
                                    <span>{value.deadline}</span> <br />
                                    <span className='font-semibold'>Price :</span> <br />
                                    <span>Rp{tempPrice},00</span> <br />
                                    <span className='font-semibold'>From :</span> <br />
                                    <span>{value.user.username}</span>
                                </div>
                            </div>
                        </div>
                        <hr className="mb-10 mt-7" />
                    </>
                    )
                })}
            </div>
        </div>
        <Footer />
        </>
    )
}