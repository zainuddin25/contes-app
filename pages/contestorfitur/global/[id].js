import Layout from '../../../components/Layout/Layout'
import HeaderContestor from '../../../components/Header/headercontestor/headercontestor'
import Footer from '../../../components/Footer/Footer'
import axios from 'axios'
import Image from 'next/image'
import { useEffect, useState } from 'react'

export default function Global () {
    const [posts, setPosts] = useState([])
    const [like, setLike] = useState(0)

    useEffect(() => {
        getAllPost()
    }, [])

    const getAllPost = async () => {
        try{
            await axios.get(`http://localhost:3222/posts`)
            .then((response) => {
                setPosts(response.data.data)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }


    return(
        <>
        
        <Layout pageTitle='Global' />
        <HeaderContestor />

        <div className='py-20 mx-20'>
                <div className="flex flex-col px-20 py-10 mx-auto rounded-sm lg:grid lg:grid-cols-3 lg:gap-4" style={{
                    boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.1)'
                }}>
                {posts?.map((values, index) => {
                    return(
                    <>
                        <div className="flex justify-center mt-8 lg:mt-0" key={index}>
                            <div className="w-full rounded-lg shadow-md">
                                <div className='px-4 py-2'>
                                    <span className='text-medium'><i class="fa-solid fa-circle-user pr-2"></i> {values?.user?.username}</span>
                                </div>
                                <div className="flex justify-center w-full rounded-lg">
                                    <Image className='flex items-center justify-center' style={{borderTopRightRadius: "5px", borderTopLeftRadius: "5px"}} src={`http://localhost:3222/files/${values?.linkPath}`} width={300} height={300}/>
                                </div>
                                <div className="flex justify-between">
                                    <p className="text-lg"><i className="pr-3 mt-4 ml-6 text-lg fa-solid fa-heart text-slate-500"></i>{like}</p>
                                    <p className='mr-6 text-lg'><i className="pt-4 pr-3 text-lg fa-solid fa-comment text-slate-500"></i>{like}</p>
                                </div>
                            </div>
                        </div>
                    </>
                    )
                })}
            </div>
        </div>

        <Footer />

        </>
    )
}