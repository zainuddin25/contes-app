import { message } from 'antd'
import axios from 'axios'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import Layout from '../../../../../components/Layout/Layout'
import useAuthenicatedPage from '../../../../../helper/Authentication'

export default function Winner () {

    const [dataContestan, setDataContestan] = useState([])

    const router = useRouter()

    useAuthenicatedPage()

    const getDataContestan = async () => {
        try{
            const { id } = router.query
            const idContest = localStorage.getItem('idContest')
            const token = localStorage.getItem('token')

            await axios.get(`http://localhost:3222/contestans/submit/${idContest}/contestan/${id}`, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                setDataContestan(response.data.data)
            })
        }catch(err){
            console.log("Error : ",err)
        }
    }

    const handleWinner = async () => {
        try{
            const { id } = router.query
            const idContest = localStorage.getItem('idContest')
            const token = localStorage.getItem('token')
            await axios.put(`http://localhost:3222/contestans/${idContest}/${id}`, {}, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                if(response.status === 200) {
                    message.success('Winner is chosen')
                    router.push(`/contestorfitur/contestcreated/detailmycontest/${idContest}`)
                    localStorage.removeItem('idContest')
                }
            })
        }catch(err) {
            console.log("Error : ", err)
        }
    }

    const handleCancel = () => {
        const idContest = localStorage.getItem('idContest')
        router.push(`/contestorfitur/contestcreated/detailmycontest/${idContest}`)
        localStorage.removeItem('idContest')
    }

    useEffect(() => {
        getDataContestan()
    }, [])

    return (
        <>
            <Layout pageTitle="Select Winner" />
            <div className='flex w-screen h-screen'>
                <div className="flex items-center justify-center w-2/3 h-auto">
                    <div className='w-auto h-auto'>
                        <Image src={`http://localhost:3222/files/${dataContestan?.linkPath}`} width={500} height={500} />
                    </div>
                </div>
                <div className='w-1/3 pt-8'>
                    <h1>Choose As Winner</h1>
                    <h1 className="pt-20 text-base">Title :</h1>
                    <p>{dataContestan.title}</p>
                    <h1 className='pt-2 text-xl'>Description :</h1>
                    <p className='w-3/4 text-base'>{dataContestan.description}</p>
                    <br />
                    <br />
                    <div className='flex'>
                        <span onClick={handleWinner} className='px-4 py-2 mr-2 text-white duration-200 bg-teal-500 border border-teal-500 rounded-sm cursor-pointer ease-in- out hover:border-teal-700 hover:bg-teal-700'>Choose As Winner</span>
                        <span onClick={handleCancel} className='px-4 py-2 ml-2 text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-sm cursor-pointer hover:bg-teal-500 hover:text-white'>Cancel</span>
                    </div>
                </div>
            </div>
        </>
    )
}