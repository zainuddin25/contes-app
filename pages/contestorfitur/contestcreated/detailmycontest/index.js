import Header from "../../../../components/Header/headercontestor/headercontestor"
import Layout from "../../../../components/Layout/Layout"
import Footer from "../../../../components/Footer/Footer"
import React, { useEffect, useState } from 'react';
import { Card, Button, Modal } from 'antd';
import { useRouter } from 'next/router';
import Image from 'next/image';
import Link from "next/link"
import axios from 'axios'
import useAuthenicatedPage from "../../../../helper/Authentication";

export default function Detail() {
    
    const [activeTabKey2, setActiveTabKey2] = useState('submits');
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [dataSubmit, setDataSubmit] = useState([])
    const [price, setPrice] = useState([])
    const [contestor, setContestor] = useState([])
    const [events, setEvents] = useState([])
    const [winner, setWinner] = useState([])
    
    const router = useRouter()

    useAuthenicatedPage()

    const getDetailContest = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/events/find/${id}`)
            .then((response) => {
                const detail = response.data.data
                const result = detail.user
                setEvents(detail)
                const tempPrice = detail.competitionPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                setPrice(tempPrice)
                setContestor(result)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    const getSubmitData = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/contestans/event/submit?id=${id}`)
            .then((response) => {
                setDataSubmit(response.data.data)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    const getWinner = async () => {
        try{
            const { id } = router.query
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            // setIdUser(decode.sub)
            await axios.get(`http://localhost:3222/contestans/winner/${id}`)
            .then((response) => {
                setWinner(response.data.data)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }
    
    useEffect(() => {
        getDetailContest()
        getSubmitData()
        getWinner()
    }, [])

    const tabListNoTitle = [
        {
            key: 'brief',
            tab: 'Brief',
        },
        {
            key: 'submits',
            tab: 'Submits',
        },
        {
            key: 'winner',
            tab: 'Winner',
        },
    ];

    const contentListNoTitle = {
        brief: 
        <>
            <div>
                <h1 className="text-2xl font-bold">Descroption Contest</h1>
                <p className="w-2/4">{events.description}</p>
                <h1 className="text-2xl font-bold">Deadline Contest</h1>
                <p>{events.deadline}</p>
            </div>
        </>,
        submits: 
        <>
            <div className="container flex flex-col mx-auto lg:grid lg:grid-cols-4 lg:gap-4">
                {dataSubmit?.map((values, index) => {
                    return (
                    <>
                        <div className="w-full my-2" key={index}>
                            <div className="flex justify-center mt-8 lg:mt-0">
                                <div className="w-full rounded-lg shadow-lg">
                                    <div className="flex justify-center w-full rounded-lg">
                                        <Image className='relative flex items-center justify-center' src={`http://localhost:3222/files/${values?.linkPath}`} width={300} height={300}/>
                                    </div>
                                    <div className='flex justify-between'>
                                        <span className='px-4 py-2 text-base'><i class="fa-solid fa-circle-user mr-2"></i> {values?.user?.username}</span>
                                        <span className='px-4 py-2'>{values?.status}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                    )
                })}
            </div>
        </>,
        winner: 
        <>
            <div className="w-4/12 mx-auto">
                <div className="w-full my-2">
                    <div className="flex flex-col justify-center mt-8 lg:mt-0">
                        <div className="w-full rounded-lg shadow-lg">
                            <div className="flex justify-center w-full rounded-lg">
                                <Image src={`http://localhost:3222/files/${winner?.linkPath}`} className='relative flex items-center justify-center' style={{borderTopRightRadius: "5px", borderTopLeftRadius: "5px"}} width={400} height={400}/>
                            </div>
                            <div className='flex justify-between'>
                                <span className='px-4 py-2 text-base'><i class="fa-solid fa-circle-user mr-2"></i> {winner?.user?.username}</span>
                                <span className='px-4 py-2'>{winner?.status}</span>
                            </div>
                        </div>
                        <div style={{
                            textAlign: 'center',
                            marginTop: "50px"
                        }}>
                            <p className='py-2 text-teal-500 duration-200 ease-in-out border border-teal-500 rounded-md cursor-pointer hover:bg-teal-500 hover:text-white'>Downlad Certificate</p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    };

    const onTab2Change = (key) => {
        setActiveTabKey2(key);
    };

    const showModal = () => {
        setIsModalVisible(true);
    };
    
      const handleOk = () => {
        setIsModalVisible(false);
    };
    
      const handleCancel = () => {
        setIsModalVisible(false);
    };

    return(
        <>
            <Layout pageTitle={"Contest Detail"} />
            <Header />
            <div className="pt-20 pb-10">
                <div className="flex justify-between w-full mx-auto lg:px-20">
                    <div className="w-[90%] px-4">
                        <h1 className="mb-3 text-2xl font-bold">{events.title}</h1>
                        <p className="font-semibold"><span className="pr-2 font-normal"><i>From</i></span>{contestor.username}</p>
                    </div>
                    <div className="w-[10%] mr-2">
                    <p className="font-bold text-right">Rp{price},00</p>
                        <div className="flex w-auto justify-right">
                            <Button onClick={showModal}>
                                Select Winner
                            </Button>
                        </div>
                    </div>
                </div>
                <div className="m-4 lg:mx-20">
                    <Card style={{ width: '100%' }} tabList={tabListNoTitle} activeTabKey={activeTabKey2}onTabChange={(key) => { onTab2Change(key) }}>
                        {contentListNoTitle[activeTabKey2]}
                    </Card>
                </div>
            </div>
            <Modal title="Choose As Winner" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} style={{ top: 20 }}
            footer={[
                <Button key="back" onClick={handleCancel}>
                  Cancel
                </Button>,
            ]}
            >
                {dataSubmit?.map((values, index) => {
                    return (
                    <>
                        <div className="flex justify-between" key={index}>
                            <span className='py-2'><i class="fa-solid fa-circle-user mr-2"></i>{values?.user?.username}</span>
                            <span className='py-2 cursor-pointer' onClick={
                                () => {
                                    const { id } = router.query
                                    const idContest = id
                                    dataSubmit?.map((values) => {
                                        localStorage.setItem('idContest', idContest)
                                    })
                                    router.push(`/contestorfitur/contestcreated/detailmycontest/winner/${values?.id}`) 
                                }}>Choose As Winner</span>
                        </div>
                    </>
                    )
                })}
            </Modal>
            <Footer />
        </>
    )
}