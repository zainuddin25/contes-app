import Layout from '../../../../components/Layout/Layout'
import { Upload, Button, Form, Input, DatePicker, InputNumber, message } from 'antd'
import axios from 'axios'
import { useState } from 'react';
import { useRouter } from 'next/router'
import useAuthenicatedPage from '../../../../helper/Authentication';
const { TextArea } = Input;

export default function CreatedContest () {
    const [path, setPath] = useState([])
    const [pathProof, setPathProof] = useState([])
    const [fileList, setFileList] = useState([])
    const [fileListProof, setFileListProof] = useState([])

    const router = useRouter()

    useAuthenicatedPage()
    
    const handlerUpload = async (args) => {
        try{
            const token = localStorage.getItem('token')
            const formData = new FormData()
            formData.append("file", args.file)
            await axios.post('http://localhost:3222/files/event', formData, {
                headers: {
                    'Authorization' : `Bearer ${token}`,
                }
            })
            .then((response) => {
                setFileList([
                    {
                        path: response.data.data,
                        status: 'done',
                        name: 'Upload Success'
                    }
                ])
                setPath(response.data.data)
                if(response.status === 201) {
                    message.success('Image Uploaded')
                }
            })
        } catch (err) {
            console.log("Wop, Error : ", err)
        }
    }
    
    const handlerProofPayment = async (args) => {
        try{
            const token = localStorage.getItem('token')
            const formData = new FormData()
            formData.append("file", args.file)
            await axios.post('http://localhost:3222/files/proof', formData, {
                headers: {
                    'Authorization' : `Bearer ${token}`,
                }
            })
            .then((response) => {
                setFileListProof([
                    {
                        path: response.data.data,
                        status: 'done',
                        name: 'Proof of payment success uploaded'
                    }
                ])
                setPathProof(response.data.data)
                if(response.status === 201) {
                    message.success('Proof Of Payment Uploaded')
                }
            })
        } catch (err) {
            console.log("Wop, Error : ", err)
        }
    }

    const handleCreateContest = async (values) => {
        try{
            const token = localStorage.getItem('token')
            const { id } = router.query
            await axios.post('http://localhost:3222/events/create',{
                title: values.title,
                competitionPrice: values.prize,
                deadline: values.deadline._d,
                description: values.description,
                thumbnail: path,
                proofOfPayment: pathProof
            }, {
                headers: {
                    'Authorization' : `Bearer ${token}`,
                }
            })
            .then((response) => {
                if(response.status === 201) {
                    message.success('Your contest has been successfully created, please make a payment')
                    router.push(`/contestorfitur/contestcreated/${id}`)
                } else {
                    message.error('Ups, something wrong')
                }
            })
        } catch (err) {
            message.error('Ups, Something Wrong')
            console.log('Error :', err)
        }
    }

    const handleCancel = () => {
        const { id } = router.query
        router.push(`/contestorfitur/contestcreated/${id}`)
    }

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return(
        <>
            <Layout pageTitle="Create Contest" />
            <div className='py-10 mx-auto bg-slate-50'>
                <div className='w-[80%] lg:w-2/4 px-6 mb-12 pt-4 mx-auto bg-white shadow-md'>
                    <h1 className='text-xl text-slate-700'>Create Contest</h1>
                    <div className=''>
                        <Form name="basic" onFinish={handleCreateContest} onFinishFailed={onFinishFailed} autoComplete="off" >
                            <h1>Title</h1>
                            <Form.Item name="title" rules={[{ required: true, message: 'Please input your title' }]}>
                                <Input />
                            </Form.Item>

                            <div className='flex'>
                                <div className='w-2/4 mr-2'>
                                    <h1>Prize</h1>
                                    <Form.Item name="prize" rules={[{ required: true, message: 'Please input your prize' }]}>
                                        <InputNumber style={{width: "100%"}} />
                                    </Form.Item>
                                </div>
                                <div className='w-2/5 ml-2'>
                                    <h1>Deadline</h1>
                                    <Form.Item name="deadline">
                                        <DatePicker />
                                    </Form.Item>
                                </div>
                            </div>

                            <h1>Description</h1>
                            <Form.Item name="description">
                                <TextArea rows={8} showCount maxLength={400} />
                            </Form.Item>

                            <div className='flex justify-center w-full'>
                                <div className='mr-4'>
                                    <Upload customRequest={handlerUpload} fileList={fileList}>
                                        <div className='px-4 py-10 mt-4 border border-dashed rounded-md'>
                                            <p>Upload Thumbnail</p>
                                        </div>
                                    </Upload>
                                </div>
                            </div>

                            <Form.Item >
                                <div className="flex mb-6">
                                    <Button type="primary" htmlType="submit" style={{marginRight: "10px"}}>
                                        Submit
                                    </Button>
                                    <Button onClick={handleCancel} style={{marginLeft: "10px"}}>
                                        Cancel
                                    </Button>
                                </div>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        </>
    )
}