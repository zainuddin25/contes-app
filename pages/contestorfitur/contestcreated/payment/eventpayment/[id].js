import { UploadOutlined } from '@ant-design/icons';
import { Button, Form, Upload, message, Modal } from 'antd';
import axios from 'axios';
import jwtDecode from 'jwt-decode'
import Layout from '../../../../../components/Layout/Layout';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

export default function Payment () {

    const [fileList, setFileList] = useState([])
    const [path, setPath] = useState([])
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [datas, setDatas] = useState([])
    const [event, setEvent] = useState([])
    const [price, setPrice] = useState([])

    const router = useRouter()

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    
    const handleOk = () => {
        try{
            setIsModalVisible(false);
            message.success('Thank you for the payment, please wait for confirmation from the Admin, the admin service will run 2x24 hours')
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            router.push(`/contestorfitur/contestcreated/payment/${decode.sub}`)
        }catch(err) {
            console.log("Error : ", err)
        }
    };

    useEffect(() => {
        getEventsData()
    }, [])

    const handlerPayment = async (values) => {
        const token = localStorage.getItem('token')
        const decode = jwtDecode(token)
        try{
            const { id } = router.query
            await axios.post(`http://localhost:3222/transactions/event-payment/${id}`, {
                proofOfPayment: path
            }, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                setDatas(response.data.data)
                if(response.status === 201) {
                    setIsModalVisible(true);
                    changeStatus()
                }
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    const getEventsData = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/events/find/${id}`)
            .then((response) => {
                setEvent(response.data.data)
                const detail = response.data.data
                const tempPrice = detail.competitionPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                setPrice(tempPrice) 
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    const changeStatus = async () => {
        try{
            const token = localStorage.getItem('token')
            const { id } = router.query
            await axios.put(`http://localhost:3222/events/update-payment-status/${id}`,{}, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                console.log(response)
            })
        }catch(err) {
            console.log(err)
        }
    }

    const handlerUploadPayment = async (args) => {
        try{
            const token = localStorage.getItem('token')
            const formData = new FormData()
            formData.append("file", args.file)
            await axios.post('http://localhost:3222/files/event-payment', formData, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                setFileList([
                    {
                        path: response.data.data,
                        status: 'done',
                        name: 'Upload Success'
                    }
                ])
                setPath(response.data.data)
                if(response.status === 201) {
                    message.success('Image Uploaded')
                }
            })
        } catch (err) {
            console.log("Wop, Error : ", err)
        }
    }

    const handleCancelUpload = () => {
        try{
            const token = localStorage.getItem('token')
            const decode = jwtDecode(token)
            router.push(`/contestorfitur/contestcreated/payment/${decode.sub}`,)
        }catch(err){
            console.log("Error : ", err)
            message.error('Ups, Something Wrong')
        }
    }

    return (
        <>
        <Layout pageTitle="Payment" />
        <div className="w-screen h-screen">
            <div className="w-2/4 h-full mx-auto shadow-lg bg-slate-50">
                <h1 className="pt-8 text-2xl text-center">Payment Event</h1>
                <p className="w-2/4 mx-auto text-center">Please make payment via the following account number : <b>2450709240</b> on behalf of <b>JustContest</b></p>
                <div className='w-3/4 mx-auto mb-4'>
                    <h1 className='font-semibold'>Title Event : <span className='font-normal'>{event.title}</span></h1>
                    <h1 className='font-semibold'>Total Prize : <span className='font-normal'>Rp{price},00</span></h1>
                </div>
                <div className="w-3/4 h-auto mx-auto">
                    <Form name="basic" onFinish={handlerPayment} onFinishFailed={onFinishFailed} autoComplete="off" >
                        <div className='flex items-center justify-center w-2/4 mx-auto mt-4 mb-6'>
                            <Upload customRequest={handlerUploadPayment} fileList={fileList}>
                                <Button style={{background: "transparent"}} icon={<UploadOutlined />}>Click to Upload Your Proof of Payment</Button>
                            </Upload>
                        </div>

                        <Form.Item >
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                            <Button onClick={handleCancelUpload} style={{marginLeft: "15px"}}>
                                Cancel
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
                <Modal title={'Thanks ' + datas.username} visible={isModalVisible} footer={[
                    <Button key="submit" type="primary" onClick={handleOk}>
                        Next
                    </Button>,
                ]}>
                    <div className='w-full h-full'>
                        <h1 className='text-4xl text-center text-teal-500'>Payment Success!</h1>
                        <div className="flex flex-col items-center justify-center w-2/4 mx-auto">
                            <i className="fa-solid fa-check border-2 border-teal-500 text-teal-500 rounded-full text-3xl py-[13px] px-[18px]"></i>
                        </div>
                        <div className='w-3/4 mx-auto mt-8'>
                            <div className='flex justify-between'>
                                <p>Title Contest :</p>
                                <p>{datas.eventTitle}</p>
                            </div>
                            <div className='flex justify-between'>
                                <p>Total Payment :</p>
                                <p>Rp{price},00</p>
                            </div>
                            <div className='flex justify-between'>
                                <p>From :</p>
                                <p>{datas.username}</p>
                            </div>
                            <hr />
                            <div className='mt-2 text-center'>
                                <p>ID Transactions</p>
                                <p>{datas.id}</p>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        </div>
        </>
    )
}