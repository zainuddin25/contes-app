import { useEffect } from 'react'

export default function Certificates () {

    useEffect(() => {
        setTimeout(() => {
            // window.print()
        }, 1000);
    }, [])

    return (
        <>
        <div className='relative w-full h-screen'>

            <div className='w-full pt-24 text-center'>
                <h1 className='text-2xl font-bold uppercase font-aboreto'>Certificates</h1>
                <p className='text-lg font-semibold uppercase font-aboreto'>of achivement</p>
                <p className='text-base font-aboreto'>Congratulations from us JustContest for a user named :</p>
                <h1 className='text-4xl underline font-parisienne underline-offset-8'>MuhZaan</h1>
                <p className='text-base font-aboreto'>Because they have won the contest:</p>
                <h1 className='text-2xl underline font-parisienne underline-offset-8'>Dignitas Academy Contest</h1>
                <h1 className='w-2/4 pt-5 mx-auto text-base font-aboreto'>
                    Hopefully with this certificate it will motivate users to stay enthusiastic about participating in existing contests and keep getting prizes
                </h1>
            </div>
            <div className='absolute w-1/4 bottom-36 left-16'>
                <div className='text-center'>
                    <p className='mb-14'>Admin</p>
                    <p className='font-semibold'>Muhammad Zainuddin Basyar</p>
                </div>
            </div>
            <div className='absolute w-1/4 bottom-36 right-16'>
                <div className='text-center'>
                    <p className='mb-14'>Admin</p>
                    <p className='font-semibold'>Muhammad Ferdiyansah</p>
                </div>
            </div>
            <span className='absolute px-20 border-t-8 border-teal-500 top-10 left-10'></span>
            <span className='absolute px-20 border-t-8 border-teal-500 top-10 right-10'></span>
            <span className='absolute pb-32 border-l-8 border-teal-500 top-10 left-10'></span>
            <span className='absolute pb-32 border-l-8 border-teal-500 top-10 right-10'></span>
            <span className='absolute px-20 border-t-8 border-teal-500 bottom-10 left-10'></span>
            <span className='absolute pt-32 border-l-8 border-teal-500 bottom-10 left-10'></span>
            <span className='absolute px-20 border-t-8 border-teal-500 bottom-10 right-10'></span>
            <span className='absolute pt-32 border-l-8 border-teal-500 bottom-10 right-10'></span>
            <span className='absolute px-[50px] border-t-4 border-teal-500 top-14 left-14'></span>
            <span className='absolute pb-[85px] border-l-4 border-teal-500 top-14 left-14'></span>
            <span className='absolute px-[50px] border-t-4 border-teal-500 top-14 right-14'></span>
            <span className='absolute pb-[85px] border-l-4 border-teal-500 top-14 right-14'></span>
            <span className='absolute px-[50px] border-t-4 border-teal-500 bottom-14 left-14'></span>
            <span className='absolute pb-[85px] border-l-4 border-teal-500 bottom-14 left-14'></span>
            <span className='absolute px-[50px] border-t-4 border-teal-500 bottom-14 right-14'></span>
            <span className='absolute pb-[85px] border-l-4 border-teal-500 bottom-14 right-14'></span>
            <div className='absolute top-0 w-full'>
                <span className='flex justify-center w-2/4 mx-auto mt-10 border-t-4 border-teal-500'></span>
                <span className='flex justify-center w-5/12 mx-auto mt-4 border-t-4 border-teal-500'></span>
                <span className='flex justify-center w-4/12 mx-auto mt-4 border-t-4 border-teal-500'></span>
            </div>
            <div className='absolute bottom-0 w-full'>
                <span className='flex justify-center w-4/12 mx-auto mb-4 border-t-4 border-teal-500'></span>
                <span className='flex justify-center w-5/12 mx-auto mb-4 border-t-4 border-teal-500'></span>
                <span className='flex justify-center w-2/4 mx-auto mb-10 border-t-4 border-teal-500'></span>
            </div>
        </div>
        </>
    )
}