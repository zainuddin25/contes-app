import Link from 'next/link'
import image from "./../../../public/logo.png"
import Image from "next/image"
import { Form, Input, Button, message } from 'antd'
import { LockOutlined } from '@ant-design/icons';
import React from 'react';
import Layout from '../../../components/Layout/Layout';
import { useRouter } from 'next/router';
import axios from 'axios';

export default function RestPass() {

    const router = useRouter()

    const resetPassword = async (values) => {
        try{
            const { id } = router.query
            const password = values.password
            await axios.put(`http://localhost:3222/auth/reset-password/${id}`, {
                password: password
            })
            .then((response) => {
                if(response.status === 200) {
                    message.success('Password Edited')
                    router.push('/Register/Login/Login')
                }
            })
        }catch(err){
            message.error(err.response.data.message[0])
            console.log("Error : ", err)
        }
    }

    return(
        <>
            <Layout pageTitle="Reset Password" />
            <div className="w-screen h-screen">
                <div className="flex items-center justify-center w-5/6 h-full mx-auto">
                    <div className="hidden w-2/4 lg:block">
                        <div className="h-full">
                            <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg" className="w-full" alt="Phone image"/>
                        </div>
                    </div>
                    <div className="w-full lg:w-2/4">
                        <div className="w-full h-auto">
                            <div className="flex justify-center w-1/4 h-auto mx-auto ">
                            <Link href="/">
                                <a>
                                    <Image src={image} width={100} height={100} className="cursor-pointer" />    
                                </a>
                            </Link>
                            </div>
                            <div className="w-3/4 mx-auto mt-8 mb-2 text-center">
                                <h1 className="text-4xl font-bold tracking-widest uppercase">reset password</h1>
                                <p clss="text-sm">
                                Enter your email and you will be sent a link to enter the contest page
                                </p>
                            </div>
                        </div>
                        <div className="w-3/4 mx-auto">
                            <Form name="normal_login" className="login-form" onFinish={resetPassword}>
                                <Form.Item name="password" rules={[{ required: true, message: 'Please input your Password!' }]}>
                                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" type="password" />} placeholder="Password" />
                                </Form.Item>
                                <div className="flex justify-center w-3/4 mx-auto">
                                    <Form.Item>
                                        <Button type="primary" htmlType="submit" className="login-form-button" style={{width: "150px"}}>
                                            Reset Password
                                        </Button>
                                    </Form.Item>
                                </div>
                                </Form>
                            <p className="text-center">
                                Back to login page?
                                <Link href="./../Login/Login">
                                    <a className="pl-1 hover:text-teal-500">Back</a>
                                </Link>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}