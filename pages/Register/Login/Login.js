import Link from 'next/link'
import image from "./../../../public/logo.png"
import Image from "next/image"
import { Form, Input, Button, message, Modal } from 'antd'
import { LockOutlined, UserOutlined, MailOutlined } from '@ant-design/icons';
import React, { useEffect, useState } from 'react';
import Layout from "./../../../components/Layout/Layout"
import axios from 'axios';
import jwtDecode from 'jwt-decode'
import { useRouter } from 'next/router';

export default function Login() {

    const [modal1Visible, setModal1Visible] = useState(false)
    // const [email, setEmail] = useState([])
    // const [code, setCode] = useState([])
    // const [password, setPassword] = useState([])

    const router = useRouter()


    const handleSubmit = async (values) => {
        try{
            await axios.post('http://localhost:3222/auth/login', {
                username: values.username,
                password: values.password
            })
            .then((response) => {
                const accessToken = response.data.accessToken
                const decode = jwtDecode(accessToken)
                const role = decode.role[0].id
                const id = decode.sub
                if(role === '3585579c-19f5-11ed-a926-14dae9af8e23') {
                    router.push(`/admin/dashboard/${id}`) 
                    localStorage.setItem('token', accessToken)
                    message.success("Welcome " + values.username)
                } else if(role === '4245cd72-19f5-11ed-a926-14dae9af8e23') {
                    router.push(`/Contestor/profile/${id}`)
                    localStorage.setItem('token', accessToken)
                    message.success("Welcome " + values.username)
                } else if(role === '42460e9d-19f5-11ed-a926-14dae9af8e23') {
                    router.push(`/contestan/profile/${id}`)
                    localStorage.setItem('token', accessToken)
                    message.success("Welcome " + values.username)
                } else {
                    message.error('Ups, something wrong')
                }
            })
        } catch (err) {
            message.error('Ups, something wrong')
            console.log('Error : ', err)
        }
    }

    const handelReset = async (values) => {
        const email = values.email
        try{
            await axios.get(`http://localhost:3222/auth/send-reset-link?email=${email}`)
            .then((response) => {
                // console.log(response)
                if(response.status === 200) {
                    message.success('Email sent please check your email')
                    setModal1Visible(false)
                }
            })
        }catch(err){
            message.error('Ups, Something Wrong')
            console.log("Error : ", err)
        }
    }

    return(
        <>
            <Layout pageTitle="Login" />
            <div className="w-screen h-screen">
                <div className="flex items-center justify-center w-5/6 h-full mx-auto">
                    <div className="hidden w-2/4 lg:block">
                        <div className="h-full">
                            <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg" className="w-full" alt="Phone image"/>
                        </div>
                    </div>
                    <div className="w-full lg:w-2/4">
                        <div className="w-full h-auto">
                            <div className="flex justify-center w-1/4 h-auto mx-auto ">
                            <Link href="/">
                                <a>
                                    <Image src={image} width={100} height={100} className="cursor-pointer" />    
                                </a>
                            </Link>
                            </div>
                            <div className="w-3/4 mx-auto mt-8 mb-2 text-center">
                                <h1 className="text-4xl font-bold tracking-widest uppercase">Welcome</h1>
                                <p clss="text-sm">
                                    Please fill in the appropriate data, if you forget your password please click the forgot password link below
                                </p>
                            </div>
                        </div>
                        <div className="w-3/4 mx-auto">
                            <Form name="normal_login" className="login-form" onFinish={handleSubmit}>
                                <Form.Item name="username" rules={[{ required: true, message: 'Please input your Username!' }]}>
                                    <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" autoComplete='off' />
                                </Form.Item>
                                <Form.Item name="password" rules={[{ required: true, message: 'Please input your Password!' }]}>
                                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="Password" />
                                </Form.Item>
                                    <div className="text-right">
                                        <Form.Item>
                                            <a className="login-form-forgot text-end" 
                                            onClick={() => setModal1Visible(true)}
                                            >
                                                Forgot password
                                            </a>
                                        </Form.Item>
                                    </div>
                                    <div className="flex justify-center w-3/4 mx-auto">
                                        <Form.Item>
                                            <Button type="primary" htmlType="submit" className="login-form-button" style={{width: "150px"}}>
                                                Log in
                                            </Button>
                                        </Form.Item>
                                    </div>
                                </Form>
                            <p className="text-center">
                                Alredy not have an account?
                                <Link href="./../Registrasi/Registrasi">
                                    <a className="pl-1 hover:text-teal-500">Sign Up</a>
                                </Link>
                            </p>
                        </div>
                        <Modal title="Enter your email for password recovery" style={{ top: 20 }} visible={modal1Visible} onOk={() => setModal1Visible(false)} onCancel={() => setModal1Visible(false)} footer={false}>
                            <Form name="normal_login" className="login-form" onFinish={handelReset}>
                                <Form.Item name="email" rules={[{ required: true, message: 'Please input your Email!', type: 'email' }]}>
                                    <Input prefix={<MailOutlined className="site-form-item-icon" />} type="email" placeholder="Email" autoComplete='off' />
                                </Form.Item>
                                <Form.Item>
                                    <Button type="primary" htmlType="submit" className="login-form-button" style={{width: "150px"}}>
                                        Send Email
                                    </Button>
                                </Form.Item>
                            </Form>
                        </Modal>
                    </div>
                </div>
            </div>
        </>
    )
}