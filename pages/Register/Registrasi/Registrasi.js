import Link from 'next/link'
import image from "./../../../public/logo.png"
import Image from "next/image"
import { Form, Input, Button, message } from 'antd'
import { MailOutlined, UserOutlined, LockOutlined } from '@ant-design/icons';
import React from 'react';
import Layout from '../../../components/Layout/Layout';
import axios from 'axios'
import { useRouter } from 'next/router'

export default function Registrasi() {
    
    const router = useRouter()

    const handleSubmit = async (values) => {
        try{
            await axios.post(`http://localhost:3222/auth/signup`, {
                username: values.username,
                email: values.email,
                password: values.password,
            })
            .then((response) => {
                if(response.status === 201) {
                    message.success('Hi, ' + values.username + ' please login to your account on the login page')
                    router.push('/Register/Login/Login')
                }else{
                    console.log(response.data.data)
                }
            })
        }catch(err){
            console.log("Error : ", err.response.data.message)
            message.error(err.response.data.message)
        }
    }

    return(
        <>
            <Layout pageTitle="Registrasi" />
            <div className="w-screen h-screen">
                <div className="flex items-center justify-center w-5/6 h-full mx-auto">
                    <div className="hidden w-2/4 lg:block">
                        <div className="h-full">
                            <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg" className="w-full" alt="Phone image"/>
                        </div>
                    </div>
                    <div className="w-full lg:w-2/4">
                        <div className="w-full h-auto">
                            <div className="flex justify-center w-1/4 h-auto mx-auto ">
                            <Link href="/">
                                <a>
                                    <Image src={image} width={100} height={100} className="cursor-pointer" />    
                                </a>
                            </Link>
                            </div>
                            <div className="w-3/4 mx-auto mt-8 mb-2 text-center">
                                <h1 className="text-4xl font-bold tracking-widest uppercase">Welcome</h1>
                                <p clss="text-sm">
                                    Enter your personal data using an email and password that is easy to remember, the password and password will be used to enter next
                                </p>
                            </div>
                        </div>
                        <div className="w-3/4 mx-auto">
                            <Form name="normal_login" className="login-form" onFinish={handleSubmit}>
                                <Form.Item name="username" rules={[{ required: true, message: 'Please input your Username!' }]}>
                                    <Input prefix={<UserOutlined className="site-form-item-icon" />} type="text" placeholder="Username" autoComplete='off' />
                                </Form.Item>
                                <Form.Item name="email" rules={[{ required: true, message: 'Please input your Email!', type: 'email' }]}>
                                    <Input prefix={<MailOutlined className="site-form-item-icon" />} type="email" placeholder="Email" autoComplete='off' />
                                </Form.Item>
                                <Form.Item name="password" rules={[{ required: true, message: 'Please input your Username!' }]}>
                                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" type="password" />} placeholder="Password" />
                                </Form.Item>
                                <div className="flex justify-center w-3/4 mx-auto">
                                    <Form.Item>
                                        <Button type="primary" htmlType="submit" className="login-form-button" style={{width: "150px"}}>
                                            Sign Up
                                        </Button>
                                    </Form.Item>
                                </div>
                            </Form>
                            <p className="text-center">
                                Already have an account?
                                <Link href="./../Login/Login">
                                    <a className="pl-1 hover:text-teal-500">Login</a>
                                </Link>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}