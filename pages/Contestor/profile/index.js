import { ExclamationCircleOutlined } from '@ant-design/icons';
import HeaderContestor from '../../../components/Header/headercontestor/headercontestor';
import Layout from '../../../components/Layout/Layout'
import Footer from '../../../components/Footer/Footer'
import { Card, Button, Form, Modal, message } from 'antd'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Image from 'next/image'
import useAuthenicatedPage from '../../../helper/Authentication';
const { confirm } = Modal;

export default function Profile() {
    
    const [activeTabKey2, setActiveTabKey2] = useState('upload');
    const [photoProfile, setPhotoProfile] = useState([])
    const [imageProfile, setImageProfile] = useState([])
    const [description, setDescription] = useState('')
    const [background, setBackground] = useState([])
    const [username, setUsername] = useState([])
    const [idUser, setIdUser] = useState([])
    const [events, setEvents] = useState([])
    const [like, setLike] = useState(0)
    const [idPost, setIdPost] = useState()
    const [accountNumber, setAccountNumber] = useState([])
    const [post, setPost] = useState([
        {
            linkPath: '',
        }
    ])

    const [form] = Form.useForm();
    const router = useRouter()

    useAuthenicatedPage()

    useEffect(() => {
        profile()
        getPathName()
        getPost()
        getEvents()
        form.setFieldsValue({
            description: description,
            username: username,
        });
    }, [])

    const profile = async () => {
        try{
            const { id } = router.query
            setIdUser(id)
            await axios.get(`http://localhost:3222/users/${id}`)
            .then((response) => {
                const data = response.data.data
                setUsername(data.username)
                setDescription(data.about)
                setBackground(data.backgroundProfile)
            })
        }catch(err) {
            console.log("Error : ", err)
        }
    }

    const getPathName = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/users/${id}`)
            .then((response) => {
                const data = response.data.data
                setPhotoProfile(data.photoProfile)
                setAccountNumber(response.data.data.accountNumber)
            })
        }catch(err) {
            console.log("Error : ", err)
        }
    }

    const getPost = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/posts/user/${id}`)
            .then((response) => {
                setPost(response.data.data)
            })
        } catch (err) {
            console.log("Error : ", err)
        }
    }

    const getEvents = async () => {
        try{
            const { id } = router.query
            await axios.get(`http://localhost:3222/events/contestor/${id}`)
            .then((response) => {
                setEvents(response.data.data)
            })
        }catch(err){
            console.log("Error : ", err)
        }
    }

    console.log(idPost)

    const onTab2Change = (key) => {
        setActiveTabKey2(key);
    };

    const handleDelete = async () => {
        try{
            const { id } = router.query
            const token = localStorage.getItem('token')
            await axios.delete(`http://localhost:3222/posts/${idPost}`, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then((response) => {
                if(response.status === 200) {
                    message.success('Delete Success')
                    router.push(`/Contestor/deleted/${id}`)
                    setIdPost([])
                }
            })
        }catch(err){
            message.error('Ups, Something Wrong, Please try again')
            console.log("Error : ", err)
        }
    }
    
    const showDeleteConfirm = () => {
        confirm({
            title: 'Are you sure delete this post?',
            icon: <ExclamationCircleOutlined />,
            content: 'If you delete this post, the post will be deleted from your profile and global posts',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',

            onOk() {
                handleDelete()
            },

            onCancel() {
                message.info('Cancel Delete')
            },
        });
    };

    const tabListNoTitle = [
        {
            key: 'about',
            tab: 'About',
        },
        {
            key: 'upload',
            tab: 'Upload',
        },
        {
            key: 'contest',
            tab: 'Contest'
        }
    ];

    const contentListNoTitle = {
        about: 
        <>
            <div className="flex flex-col">
                <div className="w-full">
                    <h1 className="text-2xl font-bold">Description</h1>
                    <p>{description}</p>
                    <h1 className='text-2xl font-bold'>Account Number</h1>
                    <p>{accountNumber}</p>
                </div>
            </div>
        </>,
        upload: 
        <>
        <div className="flex flex-col px-8 py-10 mx-auto rounded-sm lg:grid lg:grid-cols-4 lg:gap-4" style={{
            boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.1)'
        }}>
        {post?.map((values) => {
            return (
            <>
                <div className="relative flex justify-center mt-8 lg:mt-0">
                    <div className="w-full rounded-lg shadow-md">
                        <div className="flex justify-center w-full rounded-lg">
                            <Image style={{borderTopRightRadius: "5px", borderTopLeftRadius: "5px"}} src={`http://localhost:3222/files/${values?.linkPath}`} width={300} height={300} />
                        </div>
                        <div className="flex justify-between">
                            <p className="text-lg"><i className="pr-3 mt-4 ml-6 text-lg fa-solid fa-heart text-slate-500"></i>{like}</p>
                            <p className='mr-6 text-lg'><i className="pt-4 pr-3 text-lg fa-solid fa-comment text-slate-500"></i>{like}</p>
                        </div>
                    </div>
                    <div className='absolute top-0 right-0'>
                        <p onClick={() => {setIdPost(values?.id)}}><i onClick={showDeleteConfirm} className="pt-2 pr-2 ease-in-out cursor-pointer hover:opacity-90 text-md fa-solid fa-trash-can opacity-30"></i></p>
                    </div>
                </div>
            </>
            )
        })}
        </div>
        </>,
        contest:
        <>
        <div className="flex flex-col px-8 py-10 mx-auto rounded-sm lg:grid lg:grid-cols-4 lg:gap-4" style={{
            boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.1)'
        }}>
        {events?.map((values) => {
            return (
            <>
                 <div className="flex justify-center mt-8 lg:mt-0">
                    <div className="w-full rounded-lg shadow-lg">
                        <div className="flex justify-center w-full rounded-lg">
                            <Image className='flex items-center justify-center' style={{borderTopRightRadius: "5px", borderTopLeftRadius: "5px"}} src={`http://localhost:3222/files/${values?.thumbnail}`} width={300} height={300}/>
                        </div>
                        <div className="p-6">
                            <h5 className="text-xl font-medium text-gray-900">{values.title}</h5>
                            <p className="mb-2 text-gray-900">From : {values.user.username}</p>
                            <p className="w-full mb-4 text-base text-gray-700 truncate">
                                {values.description}
                            </p>
                        </div>
                    </div>
                </div>
            </>
            )
        })}
        </div>
        </>
    };
 
    return (
        <>
            <Layout pageTitle="Profile" />  
            <HeaderContestor />
            <div className='container mx-auto'>
                <div className="relative flex py-3 mx-10 bg-no-repeat bg-cover rounded-b-2xl h-52"
                    style={{
                        backgroundImage: "url(" + `http://localhost:3222/files/${background}` + ")"
                    }}
                >
                        <div className="border-4 left-4 w-auto px-[2px] bg-white border-white rounded-full absolute top-44">
                            <div className="relative">
                                <Image src={`http://localhost:3222/files/${photoProfile}`} width={110} height={110} style={{borderRadius: "100%"}}/>
                                <Link href={`/Contestor/editprofile/${idUser}`}>
                                    <div className="absolute top-20 left-[80px] cursor-pointer">
                                        <i className="px-2 py-2 text-black bg-white border rounded-full fa-solid fa-pen"></i>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        <div className="absolute top-[166px] bg-white bg-opacity-50 hover:bg-opacity-70 ease-in-out duration-200 rounded-full right-3">
                            <div>
                                <Link href={`/Contestor/editbackground/${idUser}`}>
                                    <a><i className="px-2 py-2 text-black fa-solid fa-camera"></i></a>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="flex flex-row w-full pt-1 pl-48">
                        <div className="w-2/4">
                            <span className="text-lg font-bold">{username}</span> <br />
                        </div>
                        <div className="w-2/4 pr-12 mt-1 text-right">
                            <Link href={`/Contestor/editprofile/${idUser}`}>
                                <Button>
                                    Edit Profile
                                </Button>
                            </Link>
                            <Link href={`/Contestor/uploadimage/${idUser}`}>
                                <Button type="primary" htmlType="submit" style={{marginTop: "10px", marginLeft: "10px"}}>
                                    Add
                                </Button>
                            </Link>
                        </div>
                    </div>
                    {/* Profile Content */}
                    <div className="container mx-auto">
                        <div className="mx-4 mt-10">
                            <Card bordered={false} style={{ width: '100%' }} tabList={tabListNoTitle} activeTabKey={activeTabKey2} onTabChange={(key) => { onTab2Change(key) }}>{contentListNoTitle[activeTabKey2]}</Card>
                        </div>
                    </div>
                    {/* End Profile Content */}
                </div>
                <Footer />
            </>
    )
}