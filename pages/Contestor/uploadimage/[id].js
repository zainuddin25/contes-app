import Layout from "../../../components/Layout/Layout";
import { UploadOutlined } from '@ant-design/icons';
import { Upload, Button, Form, Input, message } from "antd";
import { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import useAuthenicatedPage from "../../../helper/Authentication";

export default function UploadImage() {
    const [fileList, setFileList] = useState()
    const [path, setPath] = useState()
    
    const router = useRouter()
    const { id } = router.query
    const token = localStorage.getItem('token')

    useAuthenicatedPage()

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const handleCancel = () => {
        router.push(`/Contestor/profile/${id}`)
    }

    const uploadHandler = async (args) => {
        try{
            const formData = new FormData()
            formData.append("file", args.file)
            const result = await axios.post('http://localhost:3222/files/post', formData, {
                headers: {
                    'Content-Type' : 'multipart/form-data',
                    'Authorization' : `Bearer ${token}`,
                }
            })
            .then((res) => {
                setFileList([
                    {
                        path: res.data.data,
                        status: 'done',
                        name: 'Image Uploaded'
                    }
                ])
                setPath(res.data.data)
                // console.log(res.data.data)
                if (res.data.message === 'success') {
                    message.success(`file uploaded successfully`);
                }
            })
        } catch (err) {
            console.log('Error Brok : ', err )
        }
    }

    const handleSubmitImage = async (values) => {
        try{
            const apiUploadImage = 'http://localhost:3222/posts'
            const response = await axios.post(apiUploadImage, {
                title: values.title,
                description: values.description,
                linkPath: path,
            }, {
                headers: {
                    'Authorization' : `Bearer ${token}`
                }
            })
            if(response.status === 201) {
                router.push(`/Contestor/profile/${id}`)
            }
        } catch(err) {
            console.log('Error :', err)
        }
    }

    return (
        <>
            <Layout pageTitle='Upload Image' />
            <div className="flex flex-col w-screen h-screen lg:flex-row">
                <div className="flex items-center justify-center w-full h-1/4 lg:w-[65%] lg:h-full">
                    <Upload customRequest={(args) => uploadHandler(args)} fileList={fileList}>
                        <Button icon={<UploadOutlined />}>Click to Upload</Button>
                    </Upload>
                </div>
                <div className="w-full h-3/4 bg-slate-100 lg:w-[35%] lg:h-full">
                    <div className="flex justify-between mx-10 mt-10">
                        <p className="text-md">Upload Image</p>
                    </div>
                    <div className="mx-6">
                        <Form name="basic" onFinishFailed={onFinishFailed} onFinish={handleSubmitImage} autoComplete="off" >
                            <h1>Title</h1>
                            <Form.Item name="title" rules={[{ required: true, message: 'Please input your username!' }]}>
                                <Input />
                            </Form.Item>
                            <h1>Description</h1>
                            <Form.Item name="description" rules={[{ required: true, message: 'Please input your username!' }]}>
                                <Input.TextArea rows={8} />
                            </Form.Item>

                            <Form.Item>
                                <div className="flex">
                                    <Button type="primary" htmlType="submit" style={{marginRight: "20px"}}>
                                        Submit
                                    </Button>
                                    <Button onClick={handleCancel}>
                                        Cancel
                                    </Button>
                                </div>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        </>
    )
}