import Header from "../components/Header/headerLanding/Header"
import Hero from "./Content/Hero/Hero"
import About from "./Content/About/About"
import Tutorial from "./Content/Tutorial/Tutorial"
import Contact from "./Content/Contact/Contact"
import Footer from "../components/Footer/Footer"
import Layout from "../components/Layout/Layout"

export default function Home() {
  return (
    <>
      <Layout pageTitle="Home" />
      <Header />
      <Hero />
      <About />
      <Tutorial />
      <Contact />
      <Footer />
    </>
  )
}
