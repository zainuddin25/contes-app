/** @type {import('tailwindcss').Config} */
const defaultTheme = require("tailwindcss/defaultTheme")

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'aboreto' : ['Aboreto', ...defaultTheme.fontFamily.sans],
        'parisienne' : ['Pacifico', ...defaultTheme.fontFamily.sans]
      }
    },
  },
  plugins: [],
}
